
#include "stdafx.h"
#include <Windows.h>
#include <stdlib.h>
#include "ImageAnalyzer.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "windows.h"
#include <string>
#include <iostream>
using namespace System;
using namespace std;
using namespace cv;
/*
arg 0 = operation
	0 = analyze
	1= first run
arg 1 = flip
	0 = does not need flipped
	1= needs flipped
*/
class OpenCV;

int main(int argc, char* argv[])
{
	if(argc!=2)
		return -1;
	bool flipIt;
	OpenCV* cv = new OpenCV();
	if(atoi(argv[1])==0) //image does not need flipped
	{
		flipIt = false;
	}
	else //image needs flipped
	{
		flipIt = true;
	}
	if(atoi(argv[0])==0)//not first run
	{
		return cv->Analyze(flipIt);
	}
	else //first run
	{
		return cv->FirstRun(flipIt);
	}

}







	





		class OpenCV
		{
		public:
			OpenCV()
			{
				BaseFilename = "SeedlingAnalysisToolTempImage";
				BaseExtension = ".jpeg";
				AllDishes = "Masked";
			}
		string BaseFilename;// = "SeedlingAnalysisToolTempImage";
		string BaseExtension;// = ".jpeg";
		string AllDishes;// = "Masked";
		//input filename will be %TEMP%/SeedlingAnalysisToolTempImage.jpeg
		//output filename will be %TEMP%/SeedlingAnalysisToolTempImage.jpeg for a single image
		//output filename will be %TEMP%/SeedlingAnalysisToolTempImage#.jpeg for multiple images starting a 0

		int FirstRun(bool flipIt)
		{
			/*
			TODO: 
			first run
			raw input = first image from stack
			output = 12 color images
			12 b/w images
			1 b/w entire setup
			*/
			return 0;
		}

		int Analyze(bool flipIt)
		{
			/*
			analyze
			input = next raw image from stack
			output = 12 b/w images
			*/
			return 0;
		}

		string GetTempDirectoryPath()
		{
			string path;
			TCHAR buf [MAX_PATH];
			DWORD retVal;
			size_t size = MAX_PATH;
			char *pMBBuffer = (char *)malloc( size );
			retVal = GetTempPath(MAX_PATH, buf);
			
			if(buf != 0 && retVal <= MAX_PATH)
			{
				wcstombs_s(&size, pMBBuffer, (size_t)size,buf, (size_t)size);
				path.assign(pMBBuffer); 
				free(pMBBuffer);
			}
			return path;
		}


	
	Mat locate_grid(Mat source){

		const int RED_THRESHOLD = 220;					//	Lower threshold for red pixel value
		const int GREEN_THRESHOLD = 120;				//	Upper threshold for green pixel value
		const int BLUE_THRESHOLD = GREEN_THRESHOLD;		//	Upper threshold for blue pixel value
	 //	Variables used in grid detection and splitting algorithms
		int left = 0;									//	x coordinate for the left side of the grid containing petri dishes
		int right = 0;									//	x coordinate for the right side of the grid containing petri dishes
		int top = 0;									//	y coordinage for top of grid containing petri dishes
		int bottom = 0;									//	y coordinate for bottom of grid containing petri dishes
		int cols = source.cols;							//	columns in the image
		int rows = source.rows;							//	rows in the image
		int y = 0;										//	used in loops
		int x = 0;										//	used in loops
		int fudge = 20;									//	fudge factor for cropping image
		Vec3b bgrPixel;									//	used to access pixel data
		string path;
		 path = GetTempDirectoryPath();			//  path to temp directory, used in reading and writing files
		string fileName = path + BaseFilename + BaseExtension;		//	Name of file to use to write cropped image to disk
		IplImage * img =new IplImage(source);			// used to create cropped image
		IplImage * tmp;									// used to create cropped image
		

		//iterate through the image and locate the left edge of the grid
		for (x = 0; x < cols; x ++) {
			int y = rows>>1;
			bgrPixel = source.at<Vec3b>(y, x);
			if (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD){
				left = x;
				break;
			}//end if 
		}//end for loop
		
		//iterate through the image and locate the right edge of the grid
		for (x = cols-1; x > 0; x --) {
			y = rows>>1;
			bgrPixel = source.at<Vec3b>(y, x);
			if (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD){
				right = x;
				break;
			}//end if 
		}//end for loop
		
		//iterate through the image and locate the top edge of the grid
		for (y = 0; y < rows; y++){
			x = cols>>1;
			bgrPixel = source.at<Vec3b>(y, x);
			if (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD){
				top = y;
				break;
			}//end if 
		}//end for
		
		//iterate through the image and locate the bottom edge of the grid
		for (y = rows-1 ; y > 0; y--){
			x = cols>>1;
			bgrPixel = source.at<Vec3b>(y, x);
			if (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD){
				bottom = y;
				break;
			}//end if 
		}//end for

																// creates an image from the mat object
		
		cvSetImageROI(img, cvRect(left+fudge,top+fudge,right-left-(fudge<<1),bottom-top-(fudge<<2)));	//	sets the region of interest (the grid) inside the original image

		tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels);									//	creates a new image the size of the cropped image

		cvCopy(img, tmp, NULL);																			//	copies the cropped region to the new image

		delete(img);																					//	frees the memory occupied by img

		Mat retVal = *new Mat(tmp);																		//	creates a new Mat object to return from the method
		
		imwrite(fileName,retVal);																		//	writes file to drive

		return retVal;
 } //end locate grid


		 int  findSeedlings()
		{
			string path = GetTempDirectoryPath();
			//will load the images from findSingleDish and crop out the perimeter of the petri dish
			//will also crop the outer xx millimeters of the dish to attempt to eliminate artificial artifacting 
			//caused by the parafilm


			return 0;
		} //end findSeedlings()

		int  findAllDishes(int flipped)
		{
			string path = GetTempDirectoryPath();											//	Path to temporary Directory
			string inFileName = path + BaseFilename + BaseExtension;						//	Composited name of file to read in
			string outFileName = path + BaseFilename + AllDishes + BaseExtension;			//	Composited name of file to write to
			Mat source = imread(inFileName);												//	Mat to use for read in
			source = locate_grid(source);													//	Read In and crop image ( writes cropped image to file at inFileName )




			//will load a specified file from %temp% and process image
			





			//flipped == 0 for no mirroring needed and flipped == 1 for mirroring needed


			return 0;
		} //end findAllDishes(int flipped)

		int  findSingleDish(int flipped)
		{
			string path = GetTempDirectoryPath();
			//will load a specified file from %temp% and process the image into
			//12 images stored to the %temp% directory with a specified name format
			//can also output a text file with calibration data for each dish.
			// outer diameter of 93mm / radius x2 from hough circle detector
			//flipped == 0 for no mirroring needed and flipped == 1 for mirroring needed


			return 0;
		} //end findSingleDish(int flipped)


		};


