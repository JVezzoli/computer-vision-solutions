﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//Small class that lets me index collections using the array accessor.  Not needed, but makes code look cleaner.
namespace Seedling_Analysis_Tool
{
    class SeedlingAnalysisToolCollection<T> : IEnumerable<T>
    {
        
        private T[] arr;
        public T this[int i]
        {
            get
            {
                return arr[i];
            }
            set 
            {
                arr[i] = value;
            }
        }
        public SeedlingAnalysisToolCollection(int size)
        {
            Array.Resize(ref arr, size);
        }
        public int Count()
        {
            return arr.Count();
        }


        public IEnumerator<T> GetEnumerator()
        {
            return new SATEnum<T>(arr);
        }

        // Must also implement IEnumerable.GetEnumerator, but implement as a private method. 
        private IEnumerator GetEnumerator1()
        {
            return this.GetEnumerator();
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator1();
        }
    }

    public class SATEnum<T> : IEnumerator<T>, IDisposable
    {
        public T[] _collection;
        int position = -1;

        public SATEnum(T[] list)
        {
            _collection = list;
        }

        public bool MoveNext()
        {
            position++;
            return (position < _collection.Count());
        }

        public void Reset()
        {
            position = -1;
        }

        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }

        public T Current
        {
            get
            {
                try
                {
                    return _collection[position];
                }
                catch (IndexOutOfRangeException)
                {
                    throw new InvalidOperationException();
                }
            }
        }


        // Public implementation of Dispose pattern callable by consumers. 
        public void Dispose()
        {
            Dispose(true);
         
        }
        bool disposed = false;
        // Protected implementation of Dispose pattern. 
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                // Free any other managed objects here. 
                //
            }

            // Free any unmanaged objects here. 
            //
            disposed = true;
        }

        ~SATEnum()
        {
            
        }

    }


}
