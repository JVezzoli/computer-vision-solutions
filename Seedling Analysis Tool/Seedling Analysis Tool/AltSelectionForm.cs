﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Seedling_Analysis_Tool
{
    public partial class AltSelectionForm : Form
    {
        public FileInfo[] pictureList;
        public decimal interval;
        public AltSelectionForm()
        {
            InitializeComponent();
        }

        private void AltSelectionForm_Load(object sender, EventArgs e)
        {
            pictureBox1.Image = Image.FromFile(pictureList[0].FullName);
            pictureBox2.Image = Image.FromFile(pictureList[pictureList.Length-1].FullName);
            trackBar1.TickFrequency = 1;
            trackBar1.Maximum = pictureList.Length-1;
            trackBar2.TickFrequency = 1;
            trackBar2.Maximum = pictureList.Length-1;
            trackBar2.Value = pictureList.Length-1;
            label1.Text = pictureList[0].Name;
            label2.Text = pictureList[pictureList.Length - 1].Name;
            label1.Text = label1.Text + ": +" + trackBar1.Value * interval + "s";
            label2.Text = label2.Text + ": +" + trackBar2.Value * interval + "s";
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            pictureBox1.Image.Dispose();
            pictureBox1.Image = Image.FromFile(pictureList[trackBar1.Value].FullName);
            label1.Text = pictureList[trackBar1.Value].Name + ": +" + trackBar1.Value * interval + "s";
        }

        private void trackBar2_ValueChanged(object sender, EventArgs e)
        {
            pictureBox2.Image.Dispose();
            pictureBox2.Image = Image.FromFile(pictureList[trackBar2.Value].FullName);
            label2.Text = pictureList[trackBar2.Value].Name + ": +" + trackBar2.Value * interval + "s";
        }
    }
}
