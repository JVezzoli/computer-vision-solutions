﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Drawing.Drawing2D;

namespace Seedling_Analysis_Tool
{
    public sealed class openCVConnection
    {

#region "Private Declarations"
        private static volatile openCVConnection instance;
        private static object syncRoot = new Object();

        private static string BaseFilename = "SeedlingAnalysisToolTempImage";
        private static string BaseExtension = ".jpeg";
        private static string CalImageName = "calibration";
        private static string BaseImageNamePrefix = "Dish_";
        private static string BaseColorImageNameSufix = "_color";
        private static string CalImageNameSufix = "_calib";
        private static System.Drawing.Imaging.ImageFormat BaseFormat = System.Drawing.Imaging.ImageFormat.Jpeg;
        private static string openCVPath = System.IO.Directory.GetCurrentDirectory();
        private static string openCV = "\\ImageAnalyzer.exe";
        private openCVConnection() { }
#endregion

#region "Public Images"
        public static Image bwSetup;
        public static Image[] colorDishes;
        public static Image[] bwDishes;
        public static Image[] calImages;
#endregion        

#region "Public Methods"
        public static openCVConnection Instance
        {
            get 
            {
                if (instance == null) 
                {
                    lock (syncRoot) 
                    {
                        if (instance == null)
                            init();
                            instance = new openCVConnection();
                    }
                }

                return instance;
             }
        }

        public int analyzeSetup(String filename, int rotate)
        {
            CleanTempDir();
            int result;
            savePictureToTemp(filename);
            result = CallOpenCV(false, rotate);
            readFiles();
            return 0;
        }

        public void CleanTempDir()
        {
            disposeImages();

            string dir = Path.GetTempPath();
            String calImage = Path.Combine(dir, CalImageName + BaseExtension);
            string filename = Path.Combine(dir, BaseFilename + BaseExtension);
            if (File.Exists(filename))
                File.Delete(filename);
            if (File.Exists(calImage))
                File.Delete(calImage);
            for (int i = 0; i < 12; i++)
            {
                string imgNum = String.Format("{0:00}", i);
                string bwImage = Path.Combine(dir, BaseImageNamePrefix + imgNum + BaseExtension);
                string calImageArr = Path.Combine(dir, BaseImageNamePrefix + imgNum + CalImageNameSufix + BaseExtension);
                string colorImage = Path.Combine(dir, BaseImageNamePrefix + imgNum + BaseColorImageNameSufix + BaseExtension);
                if (File.Exists(bwImage))
                    File.Delete(bwImage);
                if (File.Exists(calImageArr))
                    File.Delete(calImageArr);
                if (File.Exists(colorImage))
                    File.Delete(colorImage);
            }
        }

        public int firstRunSetup(String filename, int rotate)
        {
            CleanTempDir();
            int result;
            savePictureToTemp(filename);
            result = CallOpenCV(true, rotate);
            readFiles(true);

            return 0;
        }

#endregion

#region "Private Methods"
        
        private static void init()
        {
            colorDishes = new Image[12];
            bwDishes = new Image[12];
            calImages = new Image[12];
        }

        private int CallOpenCV(bool firstRun, int rotate)
        {
            Process openCVProc = new Process();
            int flipBit = rotate;
            int first = 0;
            string result = "0";
            int retVal;
            
            if (firstRun)
            {
                first = 1;
            }
            
            string proc = String.Format("{0}{1}", openCVPath, openCV);
            string args = String.Format("{0} {1}", first, flipBit);
            
            openCVProc.StartInfo.Arguments = args;
            openCVProc.StartInfo.FileName = proc;
            openCVProc.StartInfo.RedirectStandardOutput = true;
            openCVProc.StartInfo.UseShellExecute = false;
            openCVProc.StartInfo.CreateNoWindow = true;
            openCVProc.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            openCVProc.Start();
            openCVProc.WaitForExit();

            bool isNumeric = int.TryParse(result, out retVal);
            if (isNumeric)
            {
                return retVal;
            }
            else
            {
                return -1;
            }
        }

        private void disposeImages()
        {
            if (bwSetup != null)
            {
                bwSetup.Dispose();
                bwSetup = null;
            }
            for (int i = 0; i < 12; i++)
            {
                if (colorDishes[i] != null)
                {
                    colorDishes[i].Dispose();
                    colorDishes[i] = null;
                }
                if (bwDishes[i] != null)
                {
                    bwDishes[i].Dispose();
                    bwDishes[i] = null;
                }
                if (calImages[i] != null)
                {
                    calImages[i].Dispose();
                    calImages[i] = null;
                }
            }
        }

        private bool readFiles(bool wasFirstRun = false)
        {

            bool status = true;
            String dir = Path.GetTempPath();

            if (wasFirstRun)
            {
                String calImage = Path.Combine(dir, CalImageName + BaseExtension);
                if (File.Exists(calImage))
                {
                    bwSetup = new Bitmap(calImage);
                }
                else
                {
                    status = false;
                }
                for (int i = 0; i < 12; i++)
                {
                    string imgNum = String.Format("{0:00}", i);//gives a digit that is two places and pads left with 0s 00, 01...11
                    string colorImage = Path.Combine(dir, BaseImageNamePrefix + imgNum + BaseColorImageNameSufix + BaseExtension);
                    if (File.Exists(colorImage))
                    {
                        if (new FileInfo(colorImage).Length != 0)
                            colorDishes[i] = ResizeImage(new Bitmap(colorImage), BaseImageNamePrefix + imgNum + BaseColorImageNameSufix + BaseExtension);
                    }
                    else
                    {
                        status = false;
                    }
                }
            }
            for (int i = 0; i < 12; i++)
            {
                string imgNum = String.Format("{0:00}", i);//gives a digit that is two places and pads left with 0s 00, 01...11
                string bwImage = Path.Combine(dir, BaseImageNamePrefix + imgNum + BaseExtension);
                if (File.Exists(bwImage))
                {
                    if (new FileInfo(bwImage).Length != 0)
                        bwDishes[i] = ResizeImage(new Bitmap(bwImage), BaseImageNamePrefix + imgNum + BaseExtension);
                }
                else
                {
                    status = false;
                }
            }
            for (int i = 0; i < 12; i++)
            {
                string imgNum = String.Format("{0:00}", i);
                string calImageArr = Path.Combine(dir, BaseImageNamePrefix + imgNum + CalImageNameSufix + BaseExtension);
                if (File.Exists(calImageArr))
                {
                    if (new FileInfo(calImageArr).Length != 0)
                        calImages[i] = ResizeImage(new Bitmap(calImageArr), BaseImageNamePrefix + imgNum + CalImageNameSufix + BaseExtension);
                }
            }
            return status;
        }

        private Bitmap ResizeImage(Bitmap bmp, string filename)
        {

            int width = UserConsts.RESIZE_IMAGE_WIDTH;
            int height = UserConsts.RESIZE_IMAGE_HEIGHT;

            Bitmap newImage = new Bitmap(width, height);
            using (Graphics gr = Graphics.FromImage(newImage))
            {
                gr.SmoothingMode = SmoothingMode.HighQuality;
                gr.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gr.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gr.DrawImage(bmp, new Rectangle(0, 0, width, height));
            }
            bmp.Dispose();
            return newImage;
        }

        private void savePictureToTemp(String sourceFilename)
        {
            string filename = Path.Combine(Path.GetTempPath(), BaseFilename + BaseExtension);
            if (File.Exists(filename))
            {
                File.Delete(filename);
            }
            File.Copy(sourceFilename, filename);
            if (File.Exists(filename))
            {
                Trace.TraceInformation(String.Format("{0} copied to temp as {1}",sourceFilename,filename));
            }
            else
            {
                Trace.TraceError(String.Format("{0} failed to copy to temp",sourceFilename));
            }
        }
#endregion
    
    }
}