﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;

namespace Seedling_Analysis_Tool
{
    class report2
    {
        const int RowOffset = 3;
        const int ColumnOffset = 5;
        const int ColumnDividor = 1;
        const int EntryColumnCount = 3;
        const int UsableValuesRowOffset = 5;
        const int UsableValuesColOffset = 1;
        
        //int TimeColumnOffset = 0;
        //int LengthColumnOffset = 1;
        //int AngleColumnOffset = 2;

        public void Print(string file, SeedlingAnalysisToolCollection<PetriDish> petridishes)
        {
            if (File.Exists(file)) File.Delete(file);
            ExcelPackage excel = new ExcelPackage(new FileInfo(file));
            try
            {
                foreach (PetriDish dish in petridishes)
                {
                    if (dish.type == "Exclude")
                        continue;

                    ExcelWorksheet ws = excel.Workbook.Worksheets.Add(dish.name + " " + dish.type);

                    int maxRows = 0;
                    int maxSeedCount = 0;
                    List<string> names = new List<string>();
                    List<DateTime> times = new List<DateTime>();

                    foreach (Seedling seed in dish.seedlings)
                    {
                        if (seed.include)
                        {
                            names.Add(seed.name);
                            if (seed.samepleCount > maxRows)
                            {
                                maxRows = seed.samepleCount;
                                times.Clear();
                                times.AddRange(seed.timestamp);
                            }
                        }
                    }
                    SetupHeaders(ws, maxRows, names.ToArray(), times.ToArray());

                    for (int row = 0; row < maxRows; row++)
                    {
                        SetupFormulas(ws, row+RowOffset);
                        int seedCount = 0;
                        int validValueCount = 0;
                        foreach (Seedling seed in dish.seedlings)
                        {

                            if (seed.include)
                            {
                                if (row <= seed.samepleCount)
                                {
                                    if (row == 0)
                                    {
                                        AddName(ws, row, seedCount, seed.name);
                                    }
                                    if (AddValues(ws, row, seedCount, seed.Length[row], seed.angle[row], seed.timestamp[row], maxRows))
                                    {
                                        validValueCount++;
                                    }
                                }

                                seedCount++;
                            }
                            if (seedCount > maxSeedCount)
                                maxSeedCount = seedCount;
                        }
                        FinishFormulas(ws, row + RowOffset, validValueCount);

                    }
                    
                    ws.Cells.AutoFitColumns();
                    AddGraphs(ws, maxRows, maxSeedCount);
                }
            }
            catch (Exception err)
            {
                Trace.TraceError(err.ToString());
                Trace.TraceError(err.StackTrace);
            }
            finally
            {
                excel.Save();
                excel.Dispose();
            }
        }

        private void AddName(ExcelWorksheet ws, int timeSlot, int seedSlot, string name)
        {
            ws.Cells[timeSlot + RowOffset - 2, ColumnOffset + seedSlot * ColumnDividor + seedSlot * EntryColumnCount].Value = name;
            ws.Cells[timeSlot + RowOffset - 1, ColumnOffset + seedSlot * ColumnDividor + seedSlot * EntryColumnCount].Value = "Time Stamp";
            ws.Cells[timeSlot + RowOffset - 1, ColumnOffset + seedSlot * ColumnDividor + seedSlot * EntryColumnCount+1].Value = "Length";
            ws.Cells[timeSlot + RowOffset - 1, ColumnOffset + seedSlot * ColumnDividor + seedSlot * EntryColumnCount+2].Value = "Angle";
        }

        private void AddGraphs(ExcelWorksheet ws, int RowCount, int seedlingCount)
        {
            if (RowCount == 0)
                return;
            LenVsTimeGraph(ws, RowCount, seedlingCount);
            AngleVsTimeGraph(ws, RowCount,seedlingCount);
        }

        private void LenVsTimeGraph(ExcelWorksheet ws, int RowCount, int seedlingCount)
        {
            ExcelChart LenVsTime = ws.Drawings.AddChart("LenVsTime", eChartType.Line);
            LenVsTime.SetPosition(RowCount + seedlingCount + UsableValuesRowOffset, 0, 2, 0);
            LenVsTime.SetSize(600, 300);
            LenVsTime.Title.Text = "Average Length by Timestamp";
            LenVsTime.XAxis.Title.Text = "Time";
            LenVsTime.YAxis.Title.Text = "Length";

            ExcelRange avgLengths = ws.Cells[RowOffset, 1,RowOffset + RowCount - 1, 1];
            ExcelRange timeStamps = ws.Cells[RowOffset, 5,RowOffset + RowCount - 1, 5];

            ExcelLineChartSerie serie = (ExcelLineChartSerie)LenVsTime.Series.Add(avgLengths, timeStamps);
            serie.Header = "Length";
        }

        private void AngleVsTimeGraph(ExcelWorksheet ws, int RowCount, int seedlingCount)
        {
            ExcelChart AngleVsTime = ws.Drawings.AddChart("AngleVsTime", eChartType.Line);
            AngleVsTime.SetPosition(RowCount + seedlingCount + UsableValuesRowOffset, 0, 12, 0);
            AngleVsTime.SetSize(600, 300);
            AngleVsTime.Title.Text = "Average Angle by Timestamp";
            AngleVsTime.XAxis.Title.Text = "Time";
            AngleVsTime.YAxis.Title.Text = "Angle";
            ExcelRange avgAngles = ws.Cells[RowOffset, 2,RowOffset + RowCount - 1, 2];
            ExcelRange timeStamps = ws.Cells[RowOffset, 5,RowOffset + RowCount - 1, 5];

            ExcelLineChartSerie serie = (ExcelLineChartSerie)AngleVsTime.Series.Add(avgAngles, timeStamps);
            serie.Header = "Angle";

        }

        private bool AddValues(ExcelWorksheet ws, int timeSlot, int seedSlot, double length, double angle, DateTime timeStamp, int maxRows)
        {
            int row = timeSlot + RowOffset;
            int col = ColumnOffset + seedSlot * ColumnDividor + seedSlot * EntryColumnCount;
            bool status = true;

            ws.Cells[row, col].Value = String.Format("{0:H:mm:ss}", timeStamp);
            if (length > 0)
            {
                ws.Cells[row, col + 1].Value = length;
                ws.Cells[row, col + 2].Value = angle;
            }
            else
            {
                status = false;
                ws.Cells[row, col + 1].Value = "";
                ws.Cells[row, col + 2].Value = "";
            }
            AppendFormulas(ws, row, GetExcelColumnName(col + 1), GetExcelColumnName(col + 2));
            AddUsableValue(ws, maxRows + UsableValuesRowOffset + seedSlot + 1, timeSlot * 2 + 1 + UsableValuesColOffset, row, GetExcelColumnName(col + 1));
            AddUsableValue(ws, maxRows + UsableValuesRowOffset + seedSlot + 1, timeSlot * 2 + 2 + UsableValuesColOffset, row, GetExcelColumnName(col + 2));
            return status;
        }

        private void AddUsableValue(ExcelWorksheet ws, int usableRow, int usableCol, int valueRow, string valueCol)
        {
            ws.Cells[usableRow, usableCol].Formula = String.Format("={0}{1}", valueCol, valueRow.ToString());
        }

        private void AppendFormulas(ExcelWorksheet ws, int row, string lenCol, string angleCol)
        {
            ws.Cells[row, 1].Formula = String.Format("{0}{1}{2},", ws.Cells[row, 1].Formula, lenCol, row);
            ws.Cells[row, 2].Formula = String.Format("{0}{1}{2},", ws.Cells[row, 2].Formula, angleCol, row);
            ws.Cells[row, 3].Formula = String.Format("{0}{1}{2},", ws.Cells[row, 3].Formula, lenCol, row);
            ws.Cells[row, 4].Formula = String.Format("{0}{1}{2},", ws.Cells[row, 4].Formula, angleCol, row);
        }

        private string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        private void SetupHeaders(ExcelWorksheet ws, int RowCount, string[] names, DateTime[] times)
        {
            ws.Cells[2, 1].Value = "Average Length";
            ws.Cells[2, 2].Value = "Average Angle";
            ws.Cells[2, 3].Value = "Length Standard Error";
            ws.Cells[2, 4].Value = "Angle Standard Error";

            for (int i = 0; i < RowCount; i++)
            {
                ws.Cells[RowCount + UsableValuesRowOffset, i * 2 + 1 + UsableValuesColOffset].Value = "Length";
                ws.Cells[RowCount + UsableValuesRowOffset - 1, i * 2 + 1 + UsableValuesColOffset].Value = String.Format("{0:H:mm:ss}", times[i]);
                ws.Cells[RowCount + UsableValuesRowOffset, i * 2 + 2 + UsableValuesColOffset].Value = "Angle";
            }

            for (int i = 0; i < names.Count(); i++)
            {
                ws.Cells[RowCount + UsableValuesRowOffset+i+1, 1].Value = names[i];
            }

        }

        private void SetupFormulas(ExcelWorksheet ws, int Row)
        {
            ws.Cells[Row, 1].Formula = "=AVERAGE(";
            ws.Cells[Row, 2].Formula = "=AVERAGE(";
            ws.Cells[Row, 3].Formula = "=STDEV(";
            ws.Cells[Row, 4].Formula = "=STDEV(";
        }

        private void FinishFormulas(ExcelWorksheet ws, int Row, int Count)
        {
            
            if (Count > 0)
            {
                ws.Cells[Row, 1].Formula = String.Format("{0})", ws.Cells[Row, 1].Formula.Substring(0, ws.Cells[Row, 1].Formula.Length - 1));
                ws.Cells[Row, 2].Formula = String.Format("{0})", ws.Cells[Row, 2].Formula.Substring(0, ws.Cells[Row, 2].Formula.Length - 1));

                //remove last comma and add )
                ws.Cells[Row, 3].Formula = String.Format("{0})", ws.Cells[Row, 3].Formula.Substring(0, ws.Cells[Row, 3].Formula.Length - 1));
                ws.Cells[Row, 4].Formula = String.Format("{0})", ws.Cells[Row, 4].Formula.Substring(0, ws.Cells[Row, 4].Formula.Length - 1));

                //add sqrt funciton
                ws.Cells[Row, 3].Formula = ws.Cells[Row, 3].Formula + "/SQRT(" + Count + ")";
                ws.Cells[Row, 4].Formula = ws.Cells[Row, 4].Formula + "/SQRT(" + Count + ")";
            }
            else
            {
                ws.Cells[Row, 1].Formula = "";
                ws.Cells[Row, 2].Formula = "";
                ws.Cells[Row, 3].Formula = "";
                ws.Cells[Row, 4].Formula = "";
            }
        }

    }
}


//int seedlingsAddedCount = 0;
//foreach (Seedling seed in dish.seedlings)
//{
//    //TODO: Add seed to tab
//    if (!seed.include)
//        continue;

//    int indexForArrays = 0;
//    int valuesAddedCount = 0;
//    foreach (Double length in seed.Length)
//    {

//        if (length > 0)
//        {
//            int row = RowOffset + valuesAddedCount;
//            int timeCol = ColumnOffset + seedlingsAddedCount * EntryColumnCount + TimeColumnOffset + seedlingsAddedCount * ColumnDividor;
//            int lenCol = ColumnOffset + seedlingsAddedCount * EntryColumnCount + LengthColumnOffset + seedlingsAddedCount * ColumnDividor;
//            int angleCol = ColumnOffset + seedlingsAddedCount * EntryColumnCount + AngleColumnOffset + seedlingsAddedCount * ColumnDividor;

//            ws.Cells[row, timeCol].Value = String.Format("{0:H:mm:ss}", seed.timestamp[indexForArrays]); ;
//            ws.Cells[row, lenCol].Value = length;
//            ws.Cells[row, angleCol].Value = seed.angle[indexForArrays];

//            valuesAddedCount++;
//        }

//        indexForArrays++;
//    }
//    seedlingsAddedCount++;
//}