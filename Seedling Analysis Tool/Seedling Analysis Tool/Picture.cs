﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Media;
using System.Linq;
using System.Text;
using System.Diagnostics;
using Seedling_Analysis_Tool;

namespace Picture//Holy image traversing, Batman! That sure is a lot of pixel navigation!
{
    //This class is an extetion to Bitmap.  These methods are accessed from the dot operator on any object of type Bitmap
    public static class BitmapExtention
    {
        static TraceSwitch ts = new TraceSwitch("Trace Switch", "Picture");

#region "Public Methods"

        /**************************************************************
         *Input: img - the current image being looked at, should be a zoomed in petri dish
         *       startLocation - the first pixel that should be looked at
         * Output: stopLocation - the farthest pixel to the left that is non black
         * Desc:  finds the farthest pixel to the left that is not black.  this can be used to find the initial location of a seedling
         *        or the end of the root from an initial point
         **************************************************************/
        public static Point findFarthestDownLeftPixel(this Bitmap bmp, Point startLocation)
        {
            Point stopLocation = new Point(startLocation.X, startLocation.Y);
            Point temp = new Point(startLocation.X, startLocation.Y);
            do
            {
                temp = findNextPixelLeft(bmp, temp);
                if (temp.X != -1)
                    stopLocation = temp;
            } while (temp.X != -1);
            if (ts.TraceVerbose)
            {
                if (UserConsts.ENABLE_DEBUG)
                    Trace.TraceInformation(String.Format("findFarthestDownLeftPixel: pixel located ({0},{1})", stopLocation.X, stopLocation.Y));
            }
            return stopLocation;
        }

        /**************************************************************
         *Input: img - the current image being looked at, should be a zoomed in petri dish
         *       startLocation - the first pixel that should be looked at
         * Output: stopLocation - the farthest pixel to the right that is non black
         * Desc:  finds the farthest pixel to the right that is not black.  this can be used to find the end of the stem from an initial point
         **************************************************************/
        public static Point findFarthestUpRightPixel(this Bitmap bmp, Point startLocation)
        {
            Point stopLocation = new Point(startLocation.X, startLocation.Y);
            Point temp = new Point(startLocation.X, startLocation.Y);
            do
            {
                temp = findNextPixelRight(bmp, temp);
                if (temp.X != -1)
                    stopLocation = temp;
            } while (temp.X != -1);
            if (ts.TraceVerbose)
            {
                if (UserConsts.ENABLE_DEBUG)
                    Trace.TraceInformation(String.Format("findFarthestUpRightPixel: pixel located ({0},{1})", stopLocation.X, stopLocation.Y));
            }
            return stopLocation;
        }

        public static Point findNewStartLocation(this Bitmap bmp, Point startLocation)
        {
            Point newStartLocation = new Point();
            Point right, left;
            int Tolerance = UserConsts.NEW_START_LOCATION_DISTANCE;
            double lengthX = 0;
            double lengthY = 0;
            for (int i = 1; i <= Tolerance; i++)
            {
                for (int j = 0; j <= Tolerance; j++)
                {
                    for (int k = 0; k <= Tolerance; k++)
                    {
                        if (Math.Abs(j) == i || Math.Abs(k) == i)//so we don't waste time checking same points
                        {//this should ballon out from center location
                            newStartLocation.X = startLocation.X + j;
                            newStartLocation.Y = startLocation.Y + k;
                            if (!isBlack(bmp, newStartLocation))
                            {
                                right = findFarthestUpRightPixel(bmp, newStartLocation);
                                left = findFarthestDownLeftPixel(bmp, newStartLocation);
                                lengthX = Math.Abs(right.X - left.X);
                                lengthY = Math.Abs(right.Y - left.Y);
                                if (lengthX >= UserConsts.MIN_SEEDLING_LEGTH_X && lengthX < UserConsts.MAX_SEEDLING_LENGTH_X && lengthY < UserConsts.MAX_SEEDLING_LENGTH_Y)//is it a good length
                                {
                                    double mid;
                                    mid = right.X - ((right.X - left.X) / 2);
                                    //find center location
                                    if (newStartLocation.X < mid)
                                    {
                                        while (newStartLocation.X < mid && newStartLocation.X != -1)
                                            newStartLocation = bmp.findNextPixelRight(newStartLocation);
                                    }
                                    else if (newStartLocation.X > mid)
                                    {
                                        while (newStartLocation.X >= mid && newStartLocation.X != -1)
                                            newStartLocation = bmp.findNextPixelLeft(newStartLocation);
                                    }
                                    if (newStartLocation.X == -1)
                                        continue;
                                    return newStartLocation;
                                }//end if lenght check
                            }//end if not black pixel
                            newStartLocation.X = startLocation.X - j;
                            newStartLocation.Y = startLocation.Y - k;
                            if (!isBlack(bmp, newStartLocation)) //LOLOLOLOL COPY/PASTA!!!!
                            {

                                right = findFarthestUpRightPixel(bmp, newStartLocation);
                                left = findFarthestDownLeftPixel(bmp, newStartLocation);
                                lengthX = Math.Abs(right.X - left.X);
                                lengthY = Math.Abs(right.Y - left.Y);
                                if (lengthX >= UserConsts.MIN_SEEDLING_LEGTH_X && lengthX < UserConsts.MAX_SEEDLING_LENGTH_X && lengthY < UserConsts.MAX_SEEDLING_LENGTH_Y)//is it a good length
                                {
                                    double mid = lengthX / 2;
                                    mid = right.X - ((right.X - left.X) / 2);
                                    //find center location
                                    if (newStartLocation.X < mid)
                                    {
                                        while (newStartLocation.X < mid && newStartLocation.X != -1)
                                            newStartLocation = bmp.findNextPixelRight(newStartLocation);
                                    }
                                    else if (newStartLocation.X > mid)
                                    {
                                        while (newStartLocation.X >= mid && newStartLocation.X != -1)
                                            newStartLocation = bmp.findNextPixelLeft(newStartLocation);
                                    }
                                    if (newStartLocation.X == -1)
                                        continue;
                                    return newStartLocation;
                                }
                            }
                        }//end j or k == i
                    }//end k loop
                }//end j loop
            }//end i loop
            return new Point(-1, -1);
        }//end function

        public static Point findNextPixelLeft(this Bitmap bmp, Point cur)
        {
            Point next = new Point();

            bool found = false;
            for (int x = 0 - UserConsts.MAX_GAP_SIZE; x <= 1; x++)
            {
                for (int y = x; y <= UserConsts.MAX_GAP_SIZE; y++)
                {
                    next.X = cur.X + x;
                    next.Y = cur.Y + y;
                    if (!(next.X == cur.X && next.Y == cur.Y))
                    {
                        if (next.X < 0)
                            next.X = 0;
                        if (next.Y < 0)
                            next.Y = 0;
                        if (next.X > bmp.Width)
                            next.X = bmp.Width;
                        if (next.Y > bmp.Height)
                            next.Y = bmp.Height;
                        if (!(isBlack(bmp, next) || IsVisted(next, bmp)))
                        {
                            y = UserConsts.MAX_GAP_SIZE + 1;
                            x = UserConsts.MAX_GAP_SIZE + 1;
                            found = true;
                            MarkVisited(next, bmp);
                        }
                    }
                }
            }
            if (!found)
            {
                next.X = -1;
                next.Y = -1;
            }
            if (ts.TraceVerbose)
            {
                if (UserConsts.ENABLE_DEBUG)
                    Trace.TraceInformation(String.Format("findNextPixelLeft: X = {0}, Y = {1}", next.X, next.Y));
            }
            return next;
        }

        public static Point findNextPixelRight(this Bitmap bmp, Point cur)
        {
            Point next = new Point();

            bool found = false;
            for (int x = UserConsts.MAX_GAP_SIZE; x >= -UserConsts.MAX_GAP_SIZE; x--)
            {
                for (int y = x; y >= -UserConsts.MAX_GAP_SIZE; y--)
                {
                    next.X = cur.X + x;
                    next.Y = cur.Y + y;
                    if (!(next.X == cur.X && next.Y == cur.Y))
                    {
                        if (next.X < 0)
                            next.X = 0;
                        if (next.Y < 0)
                            next.Y = 0;
                        if (next.X > bmp.Width)
                            next.X = bmp.Width;
                        if (next.Y > bmp.Height)
                            next.Y = bmp.Height;
                        if (!(isBlack(bmp, next) || IsVisted(next, bmp)))
                        {
                            y = -UserConsts.MAX_GAP_SIZE - 1;
                            x = -UserConsts.MAX_GAP_SIZE - 1;
                            found = true;
                            MarkVisited(next, bmp);
                        }
                    }
                }
            }
            if (!found)
            {
                next.X = -1;
                next.Y = -1;
            }
            if (ts.TraceVerbose)
            {
                if (UserConsts.ENABLE_DEBUG)
                    Trace.TraceInformation(String.Format("findNextPixelRight: X = {0}, Y = {1}", next.X, next.Y));
            }
            return next;
        }

        /**************************************************************
         *Input: img - the current image being looked at, should be a zoomed in petri dish
         *       cur - the first pixel that should be looked at
         * Output: cur - the next non black pixel.  Follows a path of right and down like reading a book.
         * Desc:  find the next pixel that is not black
         **************************************************************/
        public static Point findNextNonBlackPixel(this Bitmap bmp, Point cur)
        {
            while (isBlack(bmp, cur) && cur.Y < bmp.Height - 1 && cur.X < bmp.Width - 1 && cur.X != -1)
            {
                if (cur.X != -1)
                    cur.X++;
                if (cur.X >= bmp.Width - 1)
                {
                    cur.X = 0;
                    cur.Y++;
                }
                if (ts.TraceVerbose)
                {
                    if (UserConsts.ENABLE_DEBUG)
                        Trace.TraceError(String.Format("{0},{1}", cur.X, cur.Y));
                }
            }
            if (cur.X == bmp.Width - 1 || cur.Y == bmp.Height - 1)
            {
                cur.X = -1;
                cur.Y = -1;
            }
            if (ts.TraceVerbose)
            {
                if (UserConsts.ENABLE_DEBUG)
                    Trace.TraceInformation(String.Format("findNextNonBlackPixel: non-black pixel at ({0},{1})", cur.X, cur.Y));
            }
            return cur;
        }

        public static Point findNextNonBlackPixelReverse(this Bitmap bmp, Point cur)
        {
            while (isBlack(bmp, cur) && cur.Y > 0 && cur.X > 0 && cur.X != -1)
            {
                if (cur.X != -1)
                    cur.X--;
                if (cur.X == 0)
                {
                    cur.X = bmp.Width - 1;
                    cur.Y--;
                }
            }
            if (cur.X == 0 || cur.Y == 0)
            {
                cur.X = -1;
                cur.Y = -1;
            }

            return cur;
        }

        /*
         * create a queue
         * add point to queue Q
         * while Q is not empty
         * set n equal to last element of Q
         * if color of n is equal to the target color (white)
         * then set color of n equal to the replacement color (black) and mark as processed
         * add west point to end of Q if west not processed
         * repeat for east, north and south points
         * */
        public static Bitmap floodfill(this Bitmap img, Point pt)
        {
            return img;
            img = CreateNonIndexedImage((Image)img);
            Queue<Point> q = new Queue<Point>();
            q.Enqueue(pt);

            int count = 0;
            int tempcount = 0;
            //choose point and checks everything around it
            Point c = pt; //reassign c at end of loop to one pixel down
            //erases seedling starting at middle moving left
            while (q.Count > 0 && q.Count < 3000 && c.X < img.Width - 1)
            {
                Point n = q.Dequeue();
                c = new Point(n.X - 1, n.Y);
                Point e = new Point(c.X, c.Y + 1);
                Point f = new Point(c.X, c.Y - 1);
                Point g = new Point(c.X - 2, c.Y);
                //Point d = new Point(c.X + 1, c.Y + 1); //checks pixel diagonal to the current pixel

                if (img.GetPixel(c.X, c.Y) != Color.Black)
                {
                    img.SetPixel(c.X, c.Y, Color.Black);
                    if (!q.Contains(c))
                        q.Enqueue(c);
                }
                else
                    tempcount++;
                if (img.GetPixel(e.X, e.Y) != Color.Black)
                {
                    img.SetPixel(e.X, e.Y, Color.Black);
                    if (!q.Contains(e))
                        q.Enqueue(e);
                }
                else
                    tempcount++;
                if (img.GetPixel(f.X, f.Y) != Color.Black)
                {
                    img.SetPixel(f.X, f.Y, Color.Black);
                    if (!q.Contains(f))
                        q.Enqueue(f);
                }
                else
                    tempcount++;
                if (img.GetPixel(g.X, g.Y) != Color.Black)
                {
                    img.SetPixel(g.X, g.Y, Color.Black);
                    if (!q.Contains(g))
                        q.Enqueue(g);
                }
                else
                    tempcount++;
                if (count > 3)
                {
                    break;
                }
                else if (tempcount > 3)
                    count++;
                tempcount = 0;
            }
            q.Clear();
            q.Enqueue(pt);
            //erases seedling starting at middle moving right
            while (q.Count > 0 && q.Count < 3000 && c.X > 0)
            {
                Point n = q.Dequeue();
                c = new Point(n.X + 1, n.Y);
                Point e = new Point(c.X, c.Y + 1);
                Point f = new Point(c.X, c.Y - 1);
                Point g = new Point(c.X + 2, c.Y);
                //Point d = new Point(c.X + 1, c.Y + 1); //checks pixel diagonal to the current pixel

                if (img.GetPixel(c.X, c.Y) != Color.Black)
                {
                    img.SetPixel(c.X, c.Y, Color.Black);
                    if (!q.Contains(c))
                        q.Enqueue(c);
                }
                else
                    tempcount++;
                if (img.GetPixel(e.X, e.Y) != Color.Black)
                {
                    img.SetPixel(e.X, e.Y, Color.Black);
                    if (!q.Contains(e))
                        q.Enqueue(e);
                }
                else
                    tempcount++;
                if (img.GetPixel(f.X, f.Y) != Color.Black)
                {
                    img.SetPixel(f.X, f.Y, Color.Black);
                    if (!q.Contains(f))
                        q.Enqueue(f);
                }
                else
                    tempcount++;
                if (img.GetPixel(g.X, g.Y) != Color.Black)
                {
                    img.SetPixel(g.X, g.Y, Color.Black);
                    if (!q.Contains(g))
                        q.Enqueue(g);
                }
                else
                    tempcount++;
                if (count > 3)
                {
                    break;
                }
                else if (tempcount > 3)
                    count++;
                tempcount = 0;
            }

            //img.Save(@"C:\Users\Chris\Documents\CS425_CS499_SeniorProject\save1.jpeg");

            return img;
        }

        /**************************************************************
         *Input: img - the current image being looked at
         *       cur - the location of the image to look at
         * Output: Bool - true if the pixel being looked at is black (ff000000) 
         *                otherwise false
         * Desc:  Gets the hex value of the color of the pixel at location cur in image img
         *        then checks if it is black (ff000000)
         **************************************************************/
        public static bool isBlack(this Bitmap bmp, Point cur)
        {
            ts.Level = TraceLevel.Off;
            Color color;
            if (cur.X < 0 || cur.Y < 0 || cur.X >= bmp.Width || cur.Y >= bmp.Height)
                return true;
            else
                color = (bmp).GetPixel(cur.X, cur.Y);
            if ((Int16)color.R < UserConsts.BLACK_RED_VALUE && (Int16)color.G < UserConsts.BLACK_GREEN_VALUE && (Int16)color.B < UserConsts.BLACK_BLUE_VALUE)
                return true;
            else
                return false;
        }

#endregion

#region "Private Methods"
        
        //This makes the Bitmap editable
        private static Bitmap CreateNonIndexedImage(Image src)
        {
            Bitmap newBmp = new Bitmap(src.Width, src.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            using (Graphics gfx = Graphics.FromImage(newBmp))
            {
                gfx.DrawImage(src, 0, 0);
            }

            return newBmp;
        }

        private static bool IsVisted(Point loc, Bitmap bmp)
        {
            return ((bmp).GetPixel(loc.X, loc.Y).Name == "ffff0000");
        }

        private static void MarkVisited(Point loc, Bitmap bmp)
        {
            (bmp).SetPixel(loc.X, loc.Y, Color.Red);
        }

#endregion

    }
}
