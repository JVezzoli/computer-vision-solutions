///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														     Start Program Description        												 			 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region description

/*
ImageAnalyzer.exe

Written by: Eric Czichos
Project: Seedling Analysis Tool
Team: Computer Vision Solutions aka the Planeteers
Team Members: Jacob Vezzoli, Chris Wiedner
Date of initial completion: ~10/31/2014
Date of last modification: 10/31/2014

About:  This program is called by the Seedling Analysis Tool (hereafter SAT) via command line.  This program loads a specified file from the users temp directory
and processes the image in several stages into output files that are placed into the users temp directory for the seedling analysis tool to use. On the initial
execution of the program, it creates a calibration image, twelve color images and twelve processed images showing the seedlings present in the dishes.

The setup this program operates with is extremely specific.  It calls for a grid of 3/4" red tape on the physical setup, two layers thick.  
This setup must be in a grid of 4 dishes wide by 3 dishes high as illustrated below with the slash marks indicating the position of the grid lines.
Located inside each box will be a petri dish with a standard outer diameter of 93mm.

The number in the center of each cell below references the corresponding petri dish in the physical setup

///////////////////////////////////////////////////////////////////////////////////
//		  y00      	//		  y10      	//		  y20      	 //		   y30     	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
// x00	  00    x01	// x02	  01    x03	// x04	  02    x05	 //	x06    03   x07	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
//		  y01      	//		  y11      	//		  y21      	 //		   y31     	 //
///////////////////////////////////////////////////////////////////////////////////
//		  y02      	//		  y12      	//		  y22      	 //		   y32     	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
// x10	  04    x11	// x12	  05    x13	//	x14	  06    x15	 //	x16    07   x17	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
//		  y03      	//		  y13      	//		  y23      	 //		   y33     	 //
///////////////////////////////////////////////////////////////////////////////////
//		  y04      	//		  y14      	//		  y24      	 //		   y34     	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
// x20	  08    x21	// x22	  09    x23	//	x24	  10    x25	 //	x26    11   x27	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
//		  y05      	//		  y15      	//		  y25      	 //		   y35     	 //
///////////////////////////////////////////////////////////////////////////////////

The program first crops the image to the external gridlines. After this stage is completed, the program iteratively locates each edge inside the grid as labeled
above.  The program uses a correction factor based on image size to ensure these edges are properly spaced apart. These edges are used later to split the image 
into a format processable by the SAT. 

The program them runs a HoughCircles detection algorithm to locate the rim of each petri dish. Again there is a corretion factor tied to the size of the image to 
ensure that a petri dish is not incorrectly masked out of the image and therefore not processed. 

The results of the HoughCircle algorithm are used to create a calibration image that is then written to the temporary directory.

The results of the HoughCircle algorithm are then turned into an image mask and applied to the cropped source image and that image is processed using a
canny edge detection algorithm.

Again the HoughCircles results are used to create a masked based on a ratio between the outer size of the petri dish and the inner area to mask out the rim and
any edges introduced by parafilm lapping over the bottom surface of the petri dish. This is not perfect and may result in unnecessary edges due to laboratory assistant
accuracy and care in setting up the physical experiment and the quality of the resulting images.

Once this final masking procedure is complete the resulting image is split based upon the edges computed above into 12 separate images which are placed in the users
temporary directory for further processing by the SAT.

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

The programs calling convention specifies two flags that must be used.


The first flag (argv[1]) is used to indicate if this is the first time the program has been called by the SAT. When this is set to 1, the program will process all images and create
the color and calibration images. Any other value will cause the program only to produce the twelve images of only the seedlings.

The second flag (argv[2]) is used to indicate if the image needs to be flipped when writing to the temp directory. If this is set to 1, the image will be flipped across the x axis 
prior to being written to the temp directory.  This is to place the seedling stem on the right side of the image. This allows proper operation of the computational code in the SAT.
Any other value will produce unflipped images.

Code Attributions:
All Computer Vision related code (HoughCircles, Canny Edge Detection, Flip, imread, imwrite, Rect, setROI, clearROI, etc) are components of the OpenCV library found at OpenCV.org and
were implemented using version 2.4.9 of the library using the DLL interface.

getTempDirectoryPath() written by Jacob Vezzoli, team member of Computer Vision Solutions specifically for this project

*/

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														     End Program Description        												 			 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region includes

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <tchar.h>
#include "windows.h"

using namespace cv;
using namespace std;

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														    Method Forward Declarations        												 			 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region forwardDeclarations

 Mat locate_grid(Mat source);
 string GetTempDirectoryPath();

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														     Program Global Variables        												 			 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region globalVariables

Mat src, src_gray,src_gray_2;
Mat dst, detected_edges;
Mat detected_edges_3;

int edgeThresh = 4;
int lowThreshold = 18;
int ratio = 7;
int kernel_size = 3;
double alpha = 1.0;
//int beta = -35;
int beta = 0;
string path = GetTempDirectoryPath();

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														     Program Global Constants        												 			 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region CONSTANTS

//	Constant values used in the grid detection algorithms
const int RED_THRESHOLD      = 220;				//	Lower threshold for red pixel value
const int GREEN_THRESHOLD    = 160;				//	Upper threshold for green pixel value
const int BLUE_THRESHOLD     = 135;         	//	Upper threshold for blue pixel value
const int BLACK_THRESHOLD    = 75;              //  Upper threshold for background value
const double DISH_RATIO      = 80.5/93.0;       //  Ratio of inner zone of dish to outer rim of dish (values are in millimeters)
const string BASE_FILENAME   = "SeedlingAnalysisToolTempImage";
const string BASE_EXTENSION  = ".jpeg";
const string DISH_NAME       = "Dish_";
const string COLOR           = "_color";
const string CALIBRATE       = "_calib";

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														           Main Program Code        												 			 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main( int argc, char** argv )
{

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														   Check for Sufficient Arguments      												 			 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region argumentCount

if (argc < 3)
	{
	std::cout << "-1\n";
	cerr << "Insufficient arguments\n";
	return -1;
	}

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														Load Image from Temporary Directory          													 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region loadImage
 
  string fileName = path + BASE_FILENAME + BASE_EXTENSION;
  src = imread( fileName );
  if( !src.data )
  { return -1; }
#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//															Crop Loaded Image to Grid				        											 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  src = locate_grid(src);						// crops image to grid and returns cropped image
  
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//															Code for Find All Dishes Below		    													//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region findDishesVariables
 int rows = src.rows;									//	Number of rows in the image ( y )
 int cols = src.cols;									//	Number of columns in the image ( x )
 int x = 0;												//	For Lop Control Variable
 int y = 0;												//	For Loop Control Variable
 Vec3b bgrPixel;										//	Used to access the pixel information
 Vec3b bgrPixelNext;
 int splitHorizontalFudge = (int)(floor(cols * 0.03333));  // Calculated ratio of spacing between horizontal columns
 int splitVerticalFudge   = (int)(floor(rows * 0.05889));  // Calculated ratio of spacing between vertical rows
 int circleRadiusFudge    = (int)(floor(rows * 0.10101));  // Calculated ratio of min true circle radius based on vertical rows
 double circleVariance    = 1.04;
 int minRadius            = (int)(floor(rows * 0.08000));  // Calculated ratio of min circle radius based on vertical rows
 int houghHighThreshold   = 200;                           // High Threshold for internal canny edge detector for HoughCircles
 int houghLowAccumulator  = 100;                           // Lower threshold value for circle centers accumulator - determines min distance between centers
 int minDist = circleRadiusFudge * 2.10;                   // Minimum Distance between circle centers          
 Mat fullMask = Mat::zeros(rows,cols,CV_8UC1);             // Mat object used to compute the various masks used.

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //										Variables for the x locations needed to crop each petri dish from the main image								//
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 int X1pos = rows >> 1;										//	Middle row of dishes Y location
 int X0pos = X1pos >> 1;									//	Upper row of images Y location
 int X2pos = X0pos + X0pos + X0pos;							//	Bottom row of images Y location

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
 //  Row 0     Row 1     Row3
 int x00 = 0, x10 = 0, x20 = 0;								// Variables for storing the x values of the first edge for all three rows
 int x01 = 0, x11 = 0, x21 = 0;								// Variables for storing the x values of the second edge for all three rows
 int x02 = 0, x12 = 0, x22 = 0;								// Variables for storing the x values of the third edge for all three rows
 int x03 = 0, x13 = 0, x23 = 0;								// Variables for storing the x values of the fourth edge for all three rows
 int x04 = 0, x14 = 0, x24 = 0;								// Variables for storing the x values of the fifth edge for all three rows
 int x05 = 0, x15 = 0, x25 = 0;								// Variables for storing the x values of the sixth edge for all three rows
 int x06 = 0, x16 = 0, x26 = 0;								// Variables for storing the x values of the seventh edge for all three rows
 int x07 = 0, x17 = 0, x27 = 0;								// Variables for storing the x values of the eighth edge for all three rows

 //    Row 0     Row 1      Row3
 bool bx00 = false, bx10 = false, bx20 = false;		   	// Variables for storing if the x values of the first edge for all three rows have been found
 bool bx01 = false, bx11 = false, bx21 = false;			// Variables for storing if the x values of the second edge for all three rows have been found
 bool bx02 = false, bx12 = false, bx22 = false;			// Variables for storing if the x values of the third edge for all three rows have been found
 bool bx03 = false, bx13 = false, bx23 = false;			// Variables for storing if the x values of the fourth edge for all three rows have been found
 bool bx04 = false, bx14 = false, bx24 = false;			// Variables for storing if the x values of the fifth edge for all three rows have been found
 bool bx05 = false, bx15 = false, bx25 = false;			// Variables for storing if the x values of the sixth edge for all three rows have been found
 bool bx06 = false, bx16 = false, bx26 = false;			// Variables for storing if the x values of the seventh edge for all three rows have been found
 bool bx07 = false, bx17 = false, bx27 = false;			// Variables for storing if the x values of the eighth edge for all three rows have been found
 
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //										Variables for the y locations needed to crop each petri dish from the main image								//
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 //	  Col 0    Col 1    Col 2   Col 3
 int y00 = 0, y10 = 0, y20 = 0, y30 = 0;							// Variables for storing the y values of the first edge for all four columns
 int y01 = 0, y11 = 0, y21 = 0, y31 = 0;							// Variables for storing the y values of the second edge for all four columns
 int y02 = 0, y12 = 0, y22 = 0, y32 = 0;							// Variables for storing the y values of the third edge for all four columns
 int y03 = 0, y13 = 0, y23 = 0, y33 = 0;							// Variables for storing the y values of the fourth edge for all four columns
 int y04 = 0, y14 = 0, y24 = 0, y34 = 0;							// Variables for storing the y values of the fifth edge for all four columns
 int y05 = 0, y15 = 0, y25 = 0, y35 = 0;							// Variables for storing the y values of the siyth edge for all four columns

 //	  Col 0    Col 1    Col 2   Col 3
 bool by00 = false, by10 = false, by20 = false, by30 = false;		// Variables for storing if the by values of the first edge for all four columns
 bool by01 = false, by11 = false, by21 = false, by31 = false;		// Variables for storing if the by values of the second edge for all four columns
 bool by02 = false, by12 = false, by22 = false, by32 = false;		// Variables for storing if the by values of the third edge for all four columns
 bool by03 = false, by13 = false, by23 = false, by33 = false;		// Variables for storing if the by values of the fourth edge for all four columns
 bool by04 = false, by14 = false, by24 = false, by34 = false;		// Variables for storing if the by values of the fifth edge for all four columns
 bool by05 = false, by15 = false, by25 = false, by35 = false;		// Variables for storing if the by values of the sixth edge for all four columns

 #pragma endregion

 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//										Execution of the various for loops to iterate through image and crop out each dish								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 #pragma region findDishesCode

 /////////////////////////////////////////////////////////////////////x values for row 0///////////////////////////////////////////////////////////////////

for (x = 0; x < cols; x ++) {
			
	y = X0pos;
	bgrPixel = src.at<Vec3b>(y, x);

	//
	//edge 0
	if ((bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx00 && !bx01 && !bx02 && !bx04 && !bx05 && !bx06 && !bx07 ){
			x00 = x;
			bx00 = true;
	} else //end if

	//edge 1
	if (bx00 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx01 && !bx02 && !bx04 && !bx05 && !bx06 && !bx07 ) {
		x01 = x;
			bx01 = true;
	} else //end if

	//	edge2
	if (bx00 && bx01 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx02 && !bx04 && !bx05 && !bx06 && !bx07 ) {
		x02 = x;
			bx02 = true;
	} else //end if

	//	edge 3
	if (bx00 && bx01 && bx02 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx04 && !bx05 && !bx06 && !bx07 ) {
		x03 = x;
			bx03 = true;
	} else //end if

	//	edge 4
	if (bx00 && bx01 && bx02 && bx03 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx04 && !bx05 && !bx06 && !bx07 ) {
		x04 = x;
			bx04 = true;
	} else //end if

	//	edge 5
	if (bx00 && bx01 && bx02 && bx03 && bx04 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx05 && !bx06 && !bx07 ) {
		x05 = x;
			bx05 = true;
	} else //end if

	//	edge 6
	if (bx00 && bx01 && bx02 && bx03 && bx04 && bx05 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx06 && !bx07 ) {
		x06 = x;
			bx06 = true;
	} else //end if

		//  edge 7
	if (bx00 && bx01 && bx02 && bx03 && bx04 && bx05 && bx06 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx07 ) {
		x07 = x;
			bx07 = true;
	} // end if

}//end for loop for row 0

 /////////////////////////////////////////////////////////////////////x values for row 1///////////////////////////////////////////////////////////////////

for (x = 0; x < cols; x ++) {
			
	y = X1pos;
	bgrPixel = src.at<Vec3b>(y, x);

	//edge 0
	if ((bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx10 && !bx11 && !bx12 && !bx14 && !bx15 && !bx16 && !bx17 ){
			x10 = x;
			bx10 = true;
	} else //end if

	//edge 1
	if (bx10 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx11 && !bx12 && !bx14 && !bx15 && !bx16 && !bx17 ) {
		x11 = x;
			bx11 = true;
	} else //end if

	//	edge2
	if (bx10 && bx11 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx12 && !bx14 && !bx15 && !bx16 && !bx17 ) {
		x12 = x;
			bx12 = true;
	} else //end if

	//	edge 3
	if (bx10 && bx11 && bx12 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx14 && !bx15 && !bx16 && !bx17 ) {
		x13 = x;
			bx13 = true;
	} else //end if

	//	edge 4
	if (bx10 && bx11 && bx12 && bx13 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx14 && !bx15 && !bx16 && !bx17 ) {
		x14 = x;
			bx14 = true;
	} else //end if

	//	edge 5
	if (bx10 && bx11 && bx12 && bx13 && bx14 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx15 && !bx16 && !bx17 ) {
		x15 = x;
			bx15 = true;
	} else //end if

	//	edge 6
	if (bx10 && bx11 && bx12 && bx13 && bx14 && bx15 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx16 && !bx17 ) {
		x16 = x;
			bx16 = true;
	} else //end if

		//  edge 7
	if (bx10 && bx11 && bx12 && bx13 && bx14 && bx15 && bx16 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx17 ) {
		x17 = x;
			bx17 = true;
	} // end if

}//end for loop for row 1

 /////////////////////////////////////////////////////////////////////x values for row 2///////////////////////////////////////////////////////////////////

for (x = 0; x < cols; x ++) {
			
	y = X2pos;
	bgrPixel = src.at<Vec3b>(y, x);

	//edge 0
	if ((bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx20 && !bx21 && !bx22 && !bx24 && !bx25 && !bx26 && !bx27 ){
			x20 = x;
			bx20 = true;
	} else //end if

	//edge 1
	if (bx20 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx21 && !bx22 && !bx24 && !bx25 && !bx26 && !bx27 ) {
		x21 = x;
			bx21 = true;
	} else //end if

	//	edge2
	if (bx20 && bx21 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx22 && !bx24 && !bx25 && !bx26 && !bx27 ) {
		x22 = x;
			bx22 = true;
	} else //end if

	//	edge 3
	if (bx20 && bx21 && bx22 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx24 && !bx25 && !bx26 && !bx27 ) {
		x23 = x;
			bx23 = true;
	} else //end if

	//	edge 4
	if (bx20 && bx21 && bx22 && bx23 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx24 && !bx25 && !bx26 && !bx27 ) {
		x24 = x;
			bx24 = true;
	} else //end if

	//	edge 5
	if (bx20 && bx21 && bx22 && bx23 && bx24 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx25 && !bx26 && !bx27 ) {
		x25 = x;
			bx25 = true;
	} else //end if

	//	edge 6
	if (bx20 && bx21 && bx22 && bx23 && bx24 && bx25 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !bx26 && !bx27 ) {
		x26 = x;
			bx26 = true;
	} else //end if

		//  edge 7
	if (bx20 && bx21 && bx22 && bx23 && bx24 && bx25 && bx26 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !bx27 ) {
		x27 = x;
			bx27 = true;
	} // end if

}//end for loop for row 2

 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //									Variables for the y positions locations needed to crop each petri dish from the main image							//
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 int Y0pos = ((x00+x10+x20)/3 + (x01 + x11 + x21)/3)>>1;		//	Leftmost Column of petri dishes X location
 int Y1pos = ((x02+x12+x22)/3 + (x03 + x13 + x23)/3)>>1;		//	Inner Left Column of petri dishes X location
 int Y2pos = ((x04+x14+x24)/3 + (x05 + x15 + x25)/3)>>1;		//	Inner Right Column of petri dishes X location
 int Y3pos = ((x06+x16+x26)/3 + (x07 + x17 + x27)/3)>>1;		//  Rightmost Column of petri dishes X location

///////////////////////////////////////////////////////////////////y values for column 0//////////////////////////////////////////////////////////////////

for (y = 0; y < rows; y ++) {
			
	x = Y0pos;
	bgrPixel = src.at<Vec3b>(y, x);

	//edge 0
	if ((bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < BLACK_THRESHOLD) && !by00 && !by01 && !by02 && !by04 && !by05 ){
			y00 = y;
			by00 = true;
	} else //end if

	//edge 1
	if (by00 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by01 && !by02 && !by04 && !by05 ) {
		y01 = y;
			by01 = true;
	} else //end if

	//	edge2
	if (by00 && by01 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < BLACK_THRESHOLD) && !by02 && !by04 && !by05 ) {
		y02 = y;
			by02 = true;
	} else //end if

	//	edge 3
	if (by00 && by01 && by02 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by04 && !by05 ) {
		y03 = y;
			by03 = true;
	} else //end if

	//	edge 4
	if (by00 && by01 && by02 && by03 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by04 && !by05 ) {
		y04 = y;
			by04 = true;
	} else //end if

	//	edge 5
	if (by00 && by01 && by02 && by03 && by04 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by05 ) {
		y05 = y;
			by05 = true;
	} //end if
}//end for loop for column 0

 ///////////////////////////////////////////////////////////////////y values for column 1//////////////////////////////////////////////////////////////////

for (y = 0; y < rows; y ++) {
			
	x = Y1pos;
	bgrPixel = src.at<Vec3b>(y, x);

	//edge 0
	if ((bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by10 && !by11 && !by12 && !by14 && !by15 ){
			y10 = y;
			by10 = true;
	} else //end if

	//edge 1
	if (by10 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by11 && !by12 && !by14 && !by15 ) {
		y11 = y;
			by11 = true;
	} else //end if

	//	edge2
	if (by10 && by11 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by12 && !by14 && !by15 ) {
		y12 = y;
			by12 = true;
	} else //end if

	//	edge 3
	if (by10 && by11 && by12 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by14 && !by15 ) {
		y13 = y;
			by13 = true;
	} else //end if

	//	edge 4
	if (by10 && by11 && by12 && by13 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by14 && !by15 ) {
		y14 = y;
			by14 = true;
	} else //end if

	//	edge 5
	if (by10 && by11 && by12 && by13 && by14 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by15 ) {
		y15 = y;
			by15 = true;
	} //end if

}//end for loop for column 1

 ///////////////////////////////////////////////////////////////////y values for column 2//////////////////////////////////////////////////////////////////

for (y = 0; y < rows; y ++) {
			
	x = Y2pos;
	bgrPixel = src.at<Vec3b>(y, x);

	//edge 0
	if ((bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by20 && !by21 && !by22 && !by24 && !by25 ){
			y20 = y;
			by20 = true;
	} else //end if

	//edge 1
	if (by20 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by21 && !by22 && !by24 && !by25 ) {
		y21 = y;
			by21 = true;
	} else //end if

	//	edge2
	if (by20 && by21 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by22 && !by24 && !by25 ) {
		y22 = y;
			by22 = true;
	} else //end if

	//	edge 3
	if (by20 && by21 && by22 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by24 && !by25 ) {
		y23 = y;
			by23 = true;
	} else //end if

	//	edge 4
	if (by20 && by21 && by22 && by23 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by24 && !by25 ) {
		y24 = y;
			by24 = true;
	} else //end if

	//	edge 5
	if (by20 && by21 && by22 && by23 && by24 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by25 ) {
		y25 = y;
			by25 = true;
	}  //end if

}//end for loop for column 2

 ///////////////////////////////////////////////////////////////////y values for column 2//////////////////////////////////////////////////////////////////

for (y = 0; y < rows; y ++) {
			
	x = Y3pos;
	bgrPixel = src.at<Vec3b>(y, x);

	//edge 0
	if ((bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by30 && !by31 && !by32 && !by34 && !by35 ){
			y30 = y;
			by30 = true;
	} else //end if

	//edge 1
	if (by30 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by31 && !by32 && !by34 && !by35 ) {
		y31 = y;
			by31 = true;
	} else //end if

	//	edge2
	if (by30 && by31 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by32 && !by34 && !by35 ) {
		y32 = y;
			by32 = true;
	} else //end if

	//	edge 3
	if (by30 && by31 && by32 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by34 && !by35 ) {
		y33 = y;
			by33 = true;
	} else //end if

	//	edge 4
	if (by30 && by31 && by32 && by33 && (bgrPixel[0] < BLACK_THRESHOLD && bgrPixel[1] < BLACK_THRESHOLD && bgrPixel[2] < RED_THRESHOLD) && !by34 && !by35 ) {
		y34 = y;
			by34 = true;
	} else //end if

	//	edge 5
	if (by30 && by31 && by32 && by33 && by34 && (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD) && !by35 ) {
		y35 = y;
			by35 = true;
	}  //end if

}//end for loop for column 2

#pragma endregion

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//													Test and correct points used to split image 														//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region errorCorrection

/* 

 The following block of code uses the splitHorizontalFudge and splitVerticalFudge factors that were previously calculated based on debuging data to
 make sure the ratio correct distance between the vertical columns and horizontal rows is present.  Due to lighting variations and subsequent changes
 to BGR value fo Red, the splitting algorithm at points can misidentify edges and this code corrects these issues. 

 The fudge factors were calculated using the average of the values from several test images for the spacing between rows and columns where the edges were
 selected correctlty and an aspect ratio to vertical and horizontal size based on these values was determined and the row or column count is then multiplied by
 this aspect ratio and the floor function of that value is taken as an integer to ensure proper image splitting

 EXAMPLE:
 Term A = (x02-x01) 
 Term A results in the third vertical edge having the second edge subtracted from it

 Term B = (x02 - splitHorizontalFudge) 
 Term B results in the value of the third column minus the aspect ratio correct distance for 3/4" tape

 if A < B this means the second vertical edge is displaced to the right closer to the third vertical edge and needs to be corrected

 END EXAMPLE

 */

if ( (x02 - x01) < (x02 - splitHorizontalFudge)) { x01 = x02 - splitHorizontalFudge;}
if ( (x04 - x03) < (x04 - splitHorizontalFudge)) { x03 = x04 - splitHorizontalFudge;}
if ( (x06 - x05) < (x06 - splitHorizontalFudge)) { x05 = x06 - splitHorizontalFudge;}

if ( (x12 - x11) < (x12 - splitHorizontalFudge)) { x11 = x12 - splitHorizontalFudge;}
if ( (x14 - x13) < (x14 - splitHorizontalFudge)) { x13 = x14 - splitHorizontalFudge;}
if ( (x16 - x15) < (x16 - splitHorizontalFudge)) { x15 = x16 - splitHorizontalFudge;}

if ( (x22 - x21) < (x22 - splitHorizontalFudge)) { x21 = x22 - splitHorizontalFudge;}
if ( (x24 - x23) < (x24 - splitHorizontalFudge)) { x23 = x24 - splitHorizontalFudge;}
if ( (x26 - x25) < (x26 - splitHorizontalFudge)) { x25 = x26 - splitHorizontalFudge;}

if ( (y02 - y01) < (y02 - splitVerticalFudge))   { y01 = y02 - splitVerticalFudge;}
if ( (y04 - y03) < (y04 - splitVerticalFudge))   { y03 = y04 - splitVerticalFudge;}

if ( (y12 - y11) < (y12 - splitVerticalFudge))   { y11 = y12 - splitVerticalFudge;}
if ( (y14 - y13) < (y14 - splitVerticalFudge))   { y13 = y14 - splitVerticalFudge;}

if ( (y22 - y21) < (y22 - splitVerticalFudge))   { y21 = y22 - splitVerticalFudge;}
if ( (y24 - y23) < (y24 - splitVerticalFudge))   { y23 = y24 - splitVerticalFudge;}

if ( (y32 - y31) < (y32 - splitVerticalFudge))   { y31 = y32 - splitVerticalFudge;}
if ( (y34 - y33) < (y34 - splitVerticalFudge))   { y33 = y34 - splitVerticalFudge;}

#pragma endregion

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														      Rects used to split image        												    	    //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region Rects

/* 
below image is from description and is used to pair the coordinates into points used to create the rectangles. The left x variable and top y variable are
paired as a point in any given cell and the right x variable and bottom y variable form the other point.  These are used to create the Rect objects r00 through r11
which correspond to the given dishes
*/

///////////////////////////////////////////////////////////////////////////////////
//		  y00      	//		  y10      	//		  y20      	 //		   y30     	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
// x00	  00    x01	// x02	  01    x03	// x04	  02    x05	 //	x06    03   x07	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
//		  y01      	//		  y11      	//		  y21      	 //		   y31     	 //
///////////////////////////////////////////////////////////////////////////////////
//		  y02      	//		  y12      	//		  y22      	 //		   y32     	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
// x10	  04    x11	// x12	  05    x13	//	x14	  06    x15	 //	x16    07   x17	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
//		  y03      	//		  y13      	//		  y23      	 //		   y33     	 //
///////////////////////////////////////////////////////////////////////////////////
//		  y04      	//		  y14      	//		  y24      	 //		   y34     	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
// x20	  08    x21	// x22	  09    x23	//	x24	  10    x25	 //	x26    11   x27	 //
//		        	//		        	//		        	 //		        	 //
//		        	//		        	//		        	 //		        	 //
//		  y05      	//		  y15      	//		  y25      	 //		   y35     	 //
///////////////////////////////////////////////////////////////////////////////////



// Rects for Top Row
Rect r00 = Rect ( Point ( x00 , y00 ) , Point ( x01 , y01 ) );
Rect r01 = Rect ( Point ( x02 , y10 ) , Point ( x03 , y11 ) );
Rect r02 = Rect ( Point ( x04 , y20 ) , Point ( x05 , y21 ) );
Rect r03 = Rect ( Point ( x06 , y30 ) , Point ( x07 , y31 ) );

// Rects for Middle Row
Rect r04 = Rect ( Point ( x10 , y02 ) , Point ( x11 , y03 ) );
Rect r05 = Rect ( Point ( x12 , y12 ) , Point ( x13 , y13 ) );
Rect r06 = Rect ( Point ( x14 , y22 ) , Point ( x15 , y23 ) );
Rect r07 = Rect ( Point ( x16 , y32 ) , Point ( x17 , y33 ) );

// Rects for Bottom Row
Rect r08 = Rect ( Point ( x20 , y04 ) , Point ( x21 , y05 ) );
Rect r09 = Rect ( Point ( x22 , y14 ) , Point ( x23 , y15 ) );
Rect r10 = Rect ( Point ( x24 , y24 ) , Point ( x25 , y25 ) );
Rect r11 = Rect ( Point ( x26 , y34 ) , Point ( x27 , y35 ) );

#pragma endregion

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//						        							    Generate Color Images    		            											//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region colorImages

//first time only

if (*argv[1] == '1'){
	
	////////////////////////////////////////////////////////create source image//////////////////////////////////////////////////////////

	IplImage * img =new IplImage(src);

	/////////////////////////////////////////////////////////output first dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r00);	

	// create a new temp image the same size and depth as img
	IplImage * tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	Mat temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "00" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output second dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r01);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "01" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output third dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r02);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "02" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output fourth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r03);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "03" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output fifth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r04);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "04" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output sixth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r05);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "05" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output seventh dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r06);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "06" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output eighth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r07);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "07" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output ninth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r08);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "08" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output tenth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r09);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "09" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	///////////////////////////////////////////////////////output eleventh dish/////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r10);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "10" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output final dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r11);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "11" + COLOR + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	} // end if

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//													    	Execute HoughCircles on Image      			       											 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region HoughCircles

  cvtColor( src, src_gray, CV_BGR2GRAY );  //converts the source image to 
  vector<Vec3f> circles;         //vector to store circle center points and radii
  HoughCircles( src_gray,circles,CV_HOUGH_GRADIENT,1,minDist,houghHighThreshold,houghLowAccumulator,minRadius,0);

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														     Generate Calibration Images        				   										 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region calibrationImage

  //first execution pass only.
  //if (*argv[1] == '1')
  if(true){
	  string fileName = path + "calibration" + BASE_EXTENSION;
for( size_t i = 0; i < circles.size(); i++ )
  {
      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][2]);
	  if (radius < circleRadiusFudge)                  { radius = circleRadiusFudge; } // prevent inaccurate circle radii
	  if (radius > circleRadiusFudge * circleVariance) { radius = circleRadiusFudge; } // prevent inaccurate circle radii

      // circle outline
(circle( fullMask, center, radius, Scalar(255), 1, 8, 0 ));
   } // end for

	
	////////////////////////////////////////////////////////create source image//////////////////////////////////////////////////////////

	IplImage * img =new IplImage(fullMask);

	/////////////////////////////////////////////////////////output first dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r00);	

	// create a new temp image the same size and depth as img
	IplImage * tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	Mat temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "00" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output second dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r01);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "01" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output third dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r02);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "02" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output fourth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r03);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);

	// write image to temp directory 
	imwrite(path + DISH_NAME + "03" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output fifth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r04);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "04" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output sixth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r05);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "05" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output seventh dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r06);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "06" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output eighth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r07);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "07" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output ninth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r08);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "08" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output tenth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r09);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);

	// write image to temp directory 
	imwrite(path + DISH_NAME + "09" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	///////////////////////////////////////////////////////output eleventh dish/////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r10);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "10" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output final dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r11);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	//create new Mat object from tmp
	temp = *new Mat(tmp);
	
	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "11" + CALIBRATE + BASE_EXTENSION,temp);

	//reset the ROI on the original image
	cvResetImageROI(img);

	} // end if

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//													Generate Mask for All Dishes Prior to Splitting        												 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region maskToDishes

fullMask = Mat::zeros(rows,cols,CV_8UC1);
for( size_t i = 0; i < circles.size(); i++ )
  {
      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][2]);
	  if (radius < circleRadiusFudge)                  { radius = circleRadiusFudge; } // prevent inaccurate circle radii
	  if (radius > circleRadiusFudge * circleVariance) { radius = circleRadiusFudge; } // prevent inaccurate circle radii


      // circle outline
(circle( fullMask, center, radius, Scalar(255), -1, CV_AA, 0 ));
   } // end for

src.copyTo(dst,fullMask);                // Creates image of all dishes ready for final masking and splitting

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														Process the entire image for Petri Dishes          												 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region CannyEdgeDetection

   /// Convert the image to grayscale
    dst.convertTo(src_gray_2,-1,alpha,beta);
    blur( src_gray_2, src_gray, Size(2,2) );
	blur( src_gray, detected_edges, Size(3,1) );
  //increase overall contrast to improve image quality
  detected_edges.convertTo(detected_edges_3, -1, alpha, beta); //increase the contrast (double)
  Canny( detected_edges_3, detected_edges, lowThreshold, lowThreshold*ratio, kernel_size );

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//													Process the entire image to Mask Dish Edges out         											 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region dishRemove

  //mask out the dishes and parafilm  tRad is a ratio between expected area inside the dish seedlings will be in MM over dish size in MM times the radius
	fullMask = Mat::zeros(rows,cols,CV_8UC1);
for( size_t i = 0; i < circles.size(); i++ )
  {
      Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
      int radius = cvRound(circles[i][2]);
	  if (radius < circleRadiusFudge)                  { radius = circleRadiusFudge; } // prevent inaccurate circle radii
	  if (radius > circleRadiusFudge * circleVariance) { radius = circleRadiusFudge; } // prevent inaccurate circle radii
	  double tRad = DISH_RATIO * radius;
      // circle outline
(circle( fullMask, center, (int)tRad, Scalar(255), -1, CV_AA, 0 ));
   } // end for

detected_edges.copyTo(dst,fullMask);

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//												  Image Prepared for splitting into Single Dish Images       											 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma region splitProcessedImage

	//////////////////////////////////////////////////////// create source image//////////////////////////////////////////////////////////        

       IplImage * img = new IplImage(dst);

    /////////////////////////////////////////////////////////output first dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r00);	

	// create a new temp image the same size and depth as img
	IplImage * tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	Mat temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "00" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output second dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r01);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "01" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output third dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r02);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "02" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output fourth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r03);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "03" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output fifth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r04);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "04" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output sixth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r05);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "05" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output seventh dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r06);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "06" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output eighth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r07);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "07" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	////////////////////////////////////////////////////////output ninth dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r08);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "08" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output tenth dish//////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r09);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "09" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	///////////////////////////////////////////////////////output eleventh dish/////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r10);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "10" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

	/////////////////////////////////////////////////////////output final dish///////////////////////////////////////////////////////////

	// set Region of Interest (ROI) in IMG using the appropriate Rect object
	cvSetImageROI(img, r11);	

	// create a new temp image the same size and depth as img
	tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); 

	// copy the cropped area to tmp from img
	cvCopy(img, tmp, NULL);

	// create new Mat object from tmp
	temp = *new Mat(tmp);

	// flip the image based on flag from argv[2]
	if (*argv[2] == '1'){
		flip(temp,temp,1); // Flip around X axis
	}
	if (*argv[2] == '2'){
		flip(temp,temp,0); // Flip around Y axis
		}
	if (*argv[2] == '3'){
		flip(temp,temp,-1); // Flip around both axis (rotate 180 degrees)
		}

	// write image to temp directory 
	imwrite(path + DISH_NAME + "11" + BASE_EXTENSION,temp);

	// reset the ROI on the original image
	cvResetImageROI(img);

#pragma endregion

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//															  End processing of all Images  	 		    											 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  std::cout << "0";
  return 0;

  } //end main

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//														    Function Implementations          												 			 //
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


  // This function locates the out boundaries of the bounding grid of the physical setup
 Mat locate_grid(Mat source){

	 	//	Constant values used in the grid detection algorithms
		const int RED_THRESHOLD = 200;					//	Lower threshold for red pixel value
		const int GREEN_THRESHOLD = 160;				//	Upper threshold for green pixel value
		const int BLUE_THRESHOLD = 135;		//	Upper threshold for blue pixel value

	    //	Variables used in grid detection and splitting algorithms
		int left = 0;									//	x coordinate for the left side of the grid containing petri dishes
		int right = 0;									//	x coordinate for the right side of the grid containing petri dishes
		int top = 0;									//	y coordinage for top of grid containing petri dishes
		int bottom = 0;									//	y coordinate for bottom of grid containing petri dishes
		int cols = source.cols;
		int rows = source.rows;
		int y = 0;
		int x = 0;
		int vFudge = (int)floor(source.rows * 0.00599);
		int hFudge = (int)floor(source.cols * 0.00395);
		Vec3b bgrPixel;
		
		//iterate through the image and locate the left edge of the grid
		for (x = 0; x < cols; x ++) {
			int y = rows>>1;
			bgrPixel = source.at<Vec3b>(y, x);
			if (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD){
				left = x;
				break;
			}//end if 
		}//end for loop
		
		//iterate through the image and locate the right edge of the grid
		for (x = cols-1; x > 0; x --) {
			y = rows>>1;
			bgrPixel = source.at<Vec3b>(y, x);
			if (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD){
				right = x;
				break;
			}//end if 
		}//end for loop
		
		//iterate through the image and locate the top edge of the grid
		for (y = 0; y < rows; y++){
			x = cols>>1;
			bgrPixel = source.at<Vec3b>(y, x);
			if (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD){
				top = y;
				break;
			}//end if 
		}//end for
		
		//iterate through the image and locate the bottom edge of the grid
		for (y = rows-1 ; y > 0; y--){
			x = cols>>1;
			bgrPixel = source.at<Vec3b>(y, x);
			if (bgrPixel[0] < BLUE_THRESHOLD && bgrPixel[1] < GREEN_THRESHOLD && bgrPixel[2] > RED_THRESHOLD){
				bottom = y;
				break;
			}//end if 
		}//end for

		IplImage * img =new IplImage(source);	// creates an image from the mat object

		cvSetImageROI(img, cvRect(left + hFudge,top + vFudge,right - left - (hFudge<<1),bottom - top -(vFudge<<2)));	// sets the region of interest (the grid) inside the original image

		IplImage * tmp = cvCreateImage(cvGetSize(img),img->depth,img->nChannels); // creates a new image the size of the cropped image

		cvCopy(img, tmp, NULL);	// copies the cropped region to the new image

		delete(img);	// frees the memory occupied by img

		Mat retVal = *new Mat(tmp);	// creates a new Mat object to return from the method
		
		return retVal;
 }


 // This function returns the user's temp directory path
string GetTempDirectoryPath()
{
	string path;
	TCHAR buf [MAX_PATH];
	DWORD retVal;
	size_t size = MAX_PATH;
	char *pMBBuffer = (char *)malloc( size );
	retVal = GetTempPath(MAX_PATH, buf);
	
	if(buf != 0 && retVal <= MAX_PATH)
	{
		wcstombs_s(&size, pMBBuffer, (size_t)size,buf, (size_t)size);
		path.assign(pMBBuffer); 
		free(pMBBuffer);
	}
	return path;
}
