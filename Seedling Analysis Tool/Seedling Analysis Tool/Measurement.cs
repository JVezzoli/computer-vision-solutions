﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using Picture;
using System.Text;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;
using System.ComponentModel;
using System.Threading;

namespace Seedling_Analysis_Tool
{
    class Measurement
    {

#region "Private Declarations"
        TraceSwitch ts = new TraceSwitch("Trace Switch", "Measurement");
        private BackgroundWorker[] workerPool = new BackgroundWorker[12];
        private String[] _images;
        private openCVConnection openCV;
        private int imgCount;
        private int petriDishCount;
#endregion

#region "Public Properties"
        public String[] images
        {
            get
            {
                return _images;
            }
            set
            {
                _images = value;
                imgCount = _images.Count();
            }
        }

        public enum RotationOptions
        {
            None = 0,
            YAxis,
            XAxis,
            r180deg
        }

        private RotationOptions _rotate;
        public RotationOptions rotate
        {
            get
            {
                return _rotate;
            }
            set
            {
                _rotate = value;
            }
        }

        private SeedlingAnalysisToolCollection<PetriDish> _petriDishes;
        public SeedlingAnalysisToolCollection<PetriDish> petriDishes
        {
            get
            {
                return _petriDishes;
            }
            set
            {
                _petriDishes = value;
            }
        }

        private int _imageInterval;
        public int imageInterval
        {
            get
            {
                return _imageInterval;
            }
            set
            {
                _imageInterval = value;
            }
        }
#endregion

#region "Constructor/Destructor"
        public Measurement(String[] images, string petriDishRootName, string seedlingRootName, int imageInterval, RotationOptions rotationOption)
        {
            try
            {
                if (Thread.CurrentThread.Name == null)
                    Thread.CurrentThread.Name = "Measurement Constructor";

                ts.Level = TraceLevel.Verbose;
                Image[] dishes;
                Image[] caldishes;
                string nameSuffix;
                _rotate = rotationOption;
                if (images != null)
                {
                    _imageInterval = imageInterval;
                    imgCount = images.Count();
                    this._images = images;
                    openCV = openCVConnection.Instance;

                    openCV.firstRunSetup(_images[0], (int)_rotate);
                    Bitmap bmp = (Bitmap)openCVConnection.bwSetup;

                    caldishes = openCVConnection.calImages;
                    dishes = openCVConnection.bwDishes;
                    petriDishCount = dishes.Count();
                    _petriDishes = new SeedlingAnalysisToolCollection<PetriDish>(petriDishCount);

                    for (int i = 0; i < petriDishCount; i++)
                    {
                        nameSuffix = i.ToString();
                        nameSuffix = nameSuffix.PadLeft(3, '0');

                        BuildDishArgs args = new BuildDishArgs();
                        args.height = dishes[i].Height;
                        args.caldish = caldishes[i];
                        args.dish = dishes[i];
                        args.DishName = petriDishRootName + nameSuffix;
                        args.imgCount = imgCount;
                        args.index = i;
                        args.seedlingRootName = seedlingRootName;
                        workerPool[i] = new BackgroundWorker();
                        workerPool[i].WorkerReportsProgress = true;
                        workerPool[i].WorkerSupportsCancellation = false;
                        workerPool[i].DoWork += new DoWorkEventHandler(BuildDish_DoWork);
                        workerPool[i].RunWorkerCompleted += new RunWorkerCompletedEventHandler(BuildDish_RunworkCompleted);
                        workerPool[i].RunWorkerAsync(args);
                    }
                    if(UserConsts.ENABLE_DEBUG)
                        Trace.TraceInformation(String.Format("All threads started: {0}", DateTime.Now));
                    for (int i = 0; i < petriDishCount; i++)
                    {
                        while (workerPool[i].IsBusy)
                        {
                            Thread.Sleep(10000);//boo!! Busy waiting sucks!  Don't do this in real life.
                        }
                    }
                    if (UserConsts.ENABLE_DEBUG)
                        Trace.TraceInformation(String.Format("All threads finished: {0}", DateTime.Now));
                }
            }
            catch (Exception err)
            {
                if (UserConsts.ENABLE_DEBUG)
                {
                    Trace.TraceError(err.ToString());
                    Trace.TraceError(err.StackTrace);
                    Trace.Flush();
                }
            }
        }

        ~Measurement()
        {
            if (_images != null)
            {
                for (int i = _images.Count() - 1; i >= 0; i--)
                {
                    _images[i] = null;
                }
            }

            if (_petriDishes != null)
            {
                for (int i = petriDishes.Count() - 1; i >= 0; i--)
                {
                    _petriDishes = null;
                }
            }
        }
#endregion

#region "Build Dish Helpers"

        //this struct gets populated and passed to thread.
        struct BuildDishArgs
        {
            public string DishName;
            public Image caldish;
            public Image dish;
            public string seedlingRootName;
            public int imgCount;
            public int height;
            public int index;
        }

        //this struct gets populated by the BuildDish thread and is returned in the Result
        struct BuildDishResult
        {
            public PetriDish dish;
            public int index;
        }

        private PetriDish BuildDish(string DishName, Image caldish, Image dish, string seedlingRootName, int imgCount, int height, int i)
        {
            Point[] seedlings;
            double lengthPerPixel = findMMPerPixel((Bitmap)caldish);
            
            if (UserConsts.ENABLE_DEBUG)
                Trace.TraceInformation(String.Format("Starting find all seedlings {3}, {0}:{1}:{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, i));
            
            seedlings = findAllSeedlings((Bitmap)dish);

            if (UserConsts.ENABLE_DEBUG)
                Trace.TraceInformation(String.Format("Finished find all seedlings {3}, {0}:{1}:{2}", DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, i));
            
            int seedlingCount = seedlings.Count();
            return new PetriDish(DishName, lengthPerPixel, seedlingCount, seedlingRootName, imgCount, seedlings, height);
        }

        private void BuildDish_RunworkCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BuildDishResult result = (BuildDishResult)e.Result;
            _petriDishes[result.index] = result.dish;
        }

        private void BuildDish_DoWork(object sender, DoWorkEventArgs e)
        {
            BuildDishArgs args = (BuildDishArgs)e.Argument;
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = String.Format("Find seedlings dish {0}", args.index);

            BuildDishResult result = new BuildDishResult();
            result.dish = BuildDish(args.DishName, args.caldish, args.dish, args.seedlingRootName, args.imgCount, args.height, args.index);
            result.index = args.index;
            e.Result = result;
        }
#endregion

#region "Public Methods"

        public void Print(String filename)
        {
            Seedling_Analysis_Tool.report2 report = new Seedling_Analysis_Tool.report2();
            report.Print(filename, _petriDishes);
        }

#endregion

#region "Private Methods"

        /**************************************************************
         *Input: bmp - the current image being looked at, should be a zoomed in petri dish
         * Output: seedlingLocations array of all seedling locations  
         * Desc:  locates the left most point of all seedlings in an image
         **************************************************************/
        private Point[] findAllSeedlings(Bitmap bmp)
        {
            List<Point> seedlingLocations = new List<Point>();
            Point cur = new Point(0, 0);
            Point canidate = new Point(0, 0);
            Point left, right;
            int start = 0;

            do
            {
                Bitmap temp = new Bitmap(bmp);
                cur = temp.findNextNonBlackPixel(cur);
                if (cur.X != -1)
                {
                    bool status = true;
                    left = temp.findFarthestDownLeftPixel(cur);
                    right = temp.findFarthestUpRightPixel(cur);
                    if (left.X + UserConsts.MIN_SEEDLING_LEGTH_X > right.X || right.X - left.X > UserConsts.MAX_SEEDLING_LENGTH_X)//x values are too close, so not a seedling
                        status = false;
                    if (Math.Abs(left.Y - right.Y) > UserConsts.MAX_SEEDLING_LENGTH_Y)//y values are too far, so not a seedling
                        status = false;
                    if (status)
                    {
                        start = right.X - ((right.X - left.X) / 2);
                        canidate = cur;

                        if (cur.X < start)
                        {
                            while (canidate.X < start && canidate.X != -1)
                                canidate = temp.findNextPixelRight(canidate);
                        }
                        else
                        {
                            while (canidate.X > start && canidate.X != -1)
                                canidate = temp.findNextPixelLeft(canidate);
                        }
                        if (canidate.X != -1 && !seedlingLocations.Contains(canidate))
                        {
                            bool skip = false;
                            foreach (Point element in seedlingLocations)
                            {
                                if (TooClose(element, canidate))
                                {
                                    skip = true;
                                    break;
                                }
                            }
                            if (!skip)
                            {
                                seedlingLocations.Add(canidate);
                                bmp = bmp.floodfill(canidate);
                                if(UserConsts.ENABLE_DEBUG)
                                    Trace.TraceInformation(String.Format("{1} - Seedling at {0}", canidate, Thread.CurrentThread.Name));
                            }
                        }
                    }
                    cur.X++;
                    if (cur.X > temp.Width - 1)
                    {
                        cur.X = 0;
                        cur.Y++;
                    }
                }
                temp.Dispose();
            } while (cur.X != -1 && cur.Y <= bmp.Height - 1);

            return seedlingLocations.ToArray();
        }

        /**************************************************************
         *Input: bmp - the current image being looked at
         *       startLocation - the location of the image to look from
         * Output: angle - the angle of inflection in the growth of the stem
         * Desc:  Finds the last point in the stem and calculates the angle
         *        based on the delta X and Y from the start location
         **************************************************************/
        private double findAngleInDegrees(Bitmap bmp, Point startLocation)
        {
            Bitmap temp = new Bitmap(bmp);
            Point stopLocation = temp.findFarthestUpRightPixel(startLocation);
            int x, y;
            double angle = 0;

            x = Math.Abs(stopLocation.X - startLocation.X);
            y = Math.Abs(stopLocation.Y - startLocation.Y);
            if (x != 0)//avoid divide by 0
                angle = Math.Atan2(y, x);//keep an eye on this incase this does not return values in all quadrents, result of Atan is in radians
            angle = angle * UserConsts.RADIANS;

            if (UserConsts.ENABLE_DEBUG)
                Trace.TraceInformation(String.Format("Angle from {0} to {1} is {2}", startLocation, stopLocation, angle));
            
            return angle;
        }

        private double findDiameter(Bitmap bmp)
        {
            Bitmap temp = new Bitmap(bmp);
            Point top, bottom, cur;
            double diameter;

            cur = new Point(0, 0);
            top = temp.findNextNonBlackPixel(cur);
            
            cur = new Point(bmp.Width-1, bmp.Height-1);
            bottom = temp.findNextNonBlackPixelReverse(cur);
            
            diameter = Math.Sqrt(Math.Pow(top.X - bottom.X, 2) + Math.Pow(top.Y - bottom.Y, 2));

            if (UserConsts.ENABLE_DEBUG)
                Trace.TraceInformation(String.Format("Diameter from {0} to {1} is {2}", top, bottom, diameter));
            
            
            return diameter;
        }

        /**************************************************************
         *Input: bmp - the current image being looked at
         *       startLocation - the location of the image to look from
         *       scalar - mm/pixel value
         * Output: length - the length of the stem in mm
         * Desc:  Counts pixels along a path right and up from the start location
         *         multiplies that value by the scalar then returns the result in mm
         **************************************************************/
        private double findLengthInMM(Bitmap bmp, Point startLocation, double scalar)
        {
            Bitmap temp = new Bitmap(bmp);
            double length = 0;
            Point cur = startLocation;
            Point left = startLocation, right = startLocation;

            while (cur.X != -1)
            {
                cur = temp.findNextPixelRight(cur);
                if (cur.X != -1)
                {
                    length += Math.Sqrt(Math.Pow(cur.X - right.X, 2) + Math.Pow(cur.Y - right.Y, 2)) * scalar;
                    right = cur;
                }
            }
            cur = startLocation;
            while (cur.X != -1)
            {
                cur = temp.findNextPixelLeft(cur);
                if (cur.X != -1)
                {
                    length += Math.Sqrt(Math.Pow(left.X - cur.X, 2) + Math.Pow(left.Y - cur.Y, 2)) * scalar;
                    left = cur;
                }
            }

            if (UserConsts.ENABLE_DEBUG)
                Trace.TraceInformation(String.Format("distance from {0} to {1} is {2}", left, right, length));
            
            return length;
        }

        private double findMMPerPixel(Bitmap bmp)
        {
            if (bmp == null)
                return -1;
            double diameterPixel = findDiameter(bmp);
            double MMPerPixel = UserConsts.PETRI_DISH_OUTER_DIAMETER / diameterPixel;

            if (UserConsts.ENABLE_DEBUG)
                Trace.TraceInformation(String.Format("{1} - MM per pixel = {0}", MMPerPixel, Thread.CurrentThread.Name));
            
            return MMPerPixel;
        }

        //Step one of seedling tracking
        //this is the puzzle piecing approach
        //visually this resembles laying two images ontop of each other and moving the top image
        //until the greatest number of seedlings have a match
        //if too few are found, this will return a point at (MAX_INT, MAX_INT)
        private Point FindOffset(SeedlingAnalysisToolCollection<Seedling> KnownLocations, Point[] NewLocations)
        {
            Point Delta = new Point();
            Point testPoint = new Point();
            Point bestSampleOffset = new Point();
            int bestSampleCount = 0;
            int goal = 0;
            Point[] GoodLocations = new Point[200];
            int testRun = 0;
            int maxSearchDistance = UserConsts.SEEDLING_TRACKING_MAX_SEARCH_DISTANCE;
            for (int i = 0; i < KnownLocations.Count(); i++)
            {
                if (KnownLocations[i].include)
                {
                    GoodLocations[goal] = KnownLocations[i].location;
                    goal++;
                }
            }
            Array.Resize(ref GoodLocations, goal);
            for (int h = 0; h < GoodLocations.Count(); h++)
            {
                for (int i = 0; i < NewLocations.Count(); i++)
                {
                    Delta.X = NewLocations[i].X - GoodLocations[h].X;
                    Delta.Y = NewLocations[i].Y - GoodLocations[h].Y;
                    testRun = 0;
                    for (int j = 0; j < KnownLocations.Count(); j++)
                    {
                        if (KnownLocations[j].include)
                        {
                            testPoint.X = KnownLocations[j].location.X + Delta.X;
                            testPoint.Y = KnownLocations[j].location.Y + Delta.Y;
                            for (int k = 0; k < NewLocations.Count(); k++)
                            {
                                if (testPoint.X + maxSearchDistance > NewLocations[k].X && testPoint.X - maxSearchDistance < NewLocations[k].X)
                                {
                                    if (testPoint.Y + maxSearchDistance > NewLocations[k].Y && testPoint.Y - maxSearchDistance < NewLocations[k].Y)
                                    {
                                        testRun++;
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                    if (testRun > bestSampleCount)
                    {
                        bestSampleCount = testRun;
                        bestSampleOffset = Delta;
                    }
                    if (bestSampleCount == goal)
                    {
                        if (UserConsts.ENABLE_DEBUG)
                            Trace.TraceInformation(String.Format("Goal reached, offset is {0}", bestSampleOffset));
                        return bestSampleOffset;
                    }
                }
            }

            if ((double)bestSampleCount / (double)goal < UserConsts.TRACKING_PERCENT_MIN)
            {
                if (UserConsts.ENABLE_DEBUG)
                    Trace.TraceInformation(String.Format("Goal not reached. Found {0}, needed {1}.  Offset is Max Value", bestSampleCount, goal));
                return new Point(Int32.MaxValue, Int32.MaxValue);
            }
            else
            {
                if (UserConsts.ENABLE_DEBUG)
                    Trace.TraceInformation(String.Format("Goal not reached. Found {0}, needed {1}.  Offset is {2}", bestSampleCount, goal, bestSampleOffset));
                return bestSampleOffset;
            }
        }

        private bool TooClose(Point known, Point canidate)
        {
            bool result = false;
            double xMax, xMin, yMax, yMin;
            int tolerance = UserConsts.MIN_DISTANCE_BETWEEN_SEEDLINGS;
            xMax = known.X + tolerance;
            xMin = known.X - tolerance;
            yMax = known.Y + tolerance;
            yMin = known.Y - tolerance;
            if (canidate.X > xMin && canidate.X < xMax && canidate.Y > yMin && canidate.Y < yMax)
                result = true;
            return result;
        }

#endregion

#region "Run Measurment"

        public bool runMeasurements()
        {
            return runMeasurements(0, imgCount);
        }

        public bool runMeasurements(int startImage)
        {
            return runMeasurements(startImage, imgCount);
        }

        public bool runMeasurements(int startImage, int stopImage)
        {
            ts.Level = TraceLevel.Verbose;
            try
            {
                if (Thread.CurrentThread.Name == null)
                    Thread.CurrentThread.Name = "Run Measurements";
                if (stopImage > imgCount)
                    stopImage = imgCount;
                if (startImage > stopImage)
                    startImage = stopImage;

                //for each image do be analyzed
                for (int sample = startImage; sample <= stopImage; sample++)
                {
                    Image[] zoomDishs, calDishes;
                    openCV.analyzeSetup(_images[sample], (int)_rotate);
                    zoomDishs = openCVConnection.bwDishes;
                    calDishes = openCVConnection.calImages;

                    //for each dish in the image
                    for (int dish = 0; dish < zoomDishs.Count(); dish++)
                    {
                        if (UserConsts.ENABLE_DEBUG)
                            Trace.TraceInformation(String.Format("Begin measurement of dish {0}", dish));
                        if (petriDishes[dish].name != "Exclude")
                        {
                            if (zoomDishs[dish] == null)//bad image, so skip it and go to next dish
                                continue;//skip this dish

                            Bitmap dishBmp = new Bitmap((Bitmap)zoomDishs[dish]);
                            
                            //Find the seedlings for this image
                            Point[] Locations = findAllSeedlings(dishBmp);
                            
                            //Find offset due to seedling cluster drift
                            //first step in seedling tracking. this finds the general location of all seedlings
                            Point Delta = FindOffset(petriDishes[dish].seedlings, Locations);
                            if (Delta.X == Int32.MaxValue)//if it is bad skip this image
                                continue;//skip this dish
             
                            //for each seedling in the image
                            for (int seed = 0; seed < petriDishes[dish].seedlingCount; seed++)
                            {
                                if (petriDishes[dish].seedlings[seed].include)
                                {
                                    Point location = petriDishes[dish].seedlings[seed].location;
                                    
                                    if (UserConsts.ENABLE_DEBUG)
                                        Trace.TraceInformation(String.Format("Dish{0}: Starting seedling at {1} pre delta", dish, location));
                                    
                                    //apply offset found when doing the first step in seedling tracking
                                    location.X = location.X + Delta.X;
                                    location.Y = location.Y + Delta.Y;
                                    
                                    if (UserConsts.ENABLE_DEBUG)
                                        Trace.TraceInformation(String.Format("Dish{0}: Starting seedling at {1} post delta", dish, location));
                                    
                                    //Do step two of seedling tracking. This is to find start point for a specific seedling
                                    location = dishBmp.findNewStartLocation(location);

                                    //if we could not find a start location X and Y will be -1, so skip this seedling
                                    if (location.X == -1)
                                    {
                                        if (UserConsts.ENABLE_DEBUG)
                                            Trace.TraceWarning("Seedling not measured");
                                        continue;//skip this seedling
                                    }

                                    if (UserConsts.ENABLE_DEBUG)
                                        Trace.TraceInformation(String.Format("Dish{0}: Starting seedling at {1} post find new start location", dish, location));
                                    
                                    //get length and angle measurments
                                    double length = findLengthInMM(dishBmp, location, petriDishes[dish].mmPerPixel);
                                    double angle = findAngleInDegrees(dishBmp, location);
                                    
                                    //this next part is used to filter out measurments that were taken on images that were incomplete.
                                    //the argument is that the length and angle should always be increasing, so if they are not
                                    //then something went wrong with the process and it is not a valid sample
                                    if (sample == 0)//if zero, always store the value
                                    {
                                        petriDishes[dish].seedlings[seed].Length[sample] = length;
                                        petriDishes[dish].seedlings[seed].angle[sample] = angle;
                                    }
                                    else
                                    {
                                        bool lengthOK = false;
                                        bool angleOK = false;

                                        //for all previous samples starting at the most recent and going to 0
                                        for (int i = sample - 1; i >= 0; i--)
                                        {
                                            //if we made it to zero and zero is 0 then length can be stored
                                            if (petriDishes[dish].seedlings[seed].Length[i] == 0)
                                            {
                                                if(i==0)
                                                {
                                                    lengthOK = true;
                                                    break;
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                            //if we are not at zero and the length we measured is larger than previous, then we should store it
                                            if (petriDishes[dish].seedlings[seed].Length[sample] < length)
                                            {
                                                lengthOK = true;
                                                break;
                                            }
                                            else//if length is shorter than the previous non-zero sample then something went wrong and we should toss it out
                                            {
                                                break;
                                            }
                                        }

                                        //do the same thing for angle
                                        for (int i = sample - 1; i >= 0; i--)
                                        {

                                            if (petriDishes[dish].seedlings[seed].angle[i] == 0)
                                            {
                                                if (i == 0)
                                                {
                                                    angleOK = true;
                                                    break;
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                            if (petriDishes[dish].seedlings[seed].angle[sample] < angle)
                                            {
                                                angleOK = true;
                                                break;
                                            }
                                            else
                                            {
                                                break;
                                            }
                                        }

                                        //if both ok, store them
                                        if (lengthOK && angleOK)
                                        {
                                            petriDishes[dish].seedlings[seed].Length[sample] = length;
                                            petriDishes[dish].seedlings[seed].angle[sample] = angle;
                                        }

                                        //if the user does not want to enforce one and other is OK, then store them
                                        if (!UserConsts.FORCE_INCREASING_ANGLE && lengthOK || !UserConsts.FORCE_INCREASING_LENGTH && angleOK)
                                        {
                                            petriDishes[dish].seedlings[seed].angle[sample] = angle;
                                            petriDishes[dish].seedlings[seed].Length[sample] = length;
                                        }

                                        //if user does not want to enforce either, then store them anyway
                                        if (!UserConsts.FORCE_INCREASING_LENGTH && !UserConsts.FORCE_INCREASING_ANGLE)
                                        {
                                            petriDishes[dish].seedlings[seed].Length[sample] = length;
                                            petriDishes[dish].seedlings[seed].angle[sample] = angle;
                                        }
                                    }

                                    petriDishes[dish].seedlings[seed].timestamp[sample] = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, _imageInterval * sample, 0);
                                }
                            }
                            dishBmp.Dispose();
                        }
                    }
                }
            }
            catch (Exception err)
            {
                if (UserConsts.ENABLE_DEBUG)
                {
                    Trace.TraceError(err.ToString());
                    Trace.TraceError(err.StackTrace);
                }
            }

            return true;
        }

#endregion

    }
}



