﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Seedling_Analysis_Tool
{
    class Seedling
    {

#region "Properties"
        private SeedlingAnalysisToolCollection<DateTime> _timestamp;
        public SeedlingAnalysisToolCollection<DateTime> timestamp
        {
            get
            {
                return _timestamp;
            }
            set
            {
                _timestamp = value;
            }
        }

        private Point _location;
        public Point location
        {
            get
            {
                return _location;
            }
        }

        private SeedlingAnalysisToolCollection<double> _Length;
        public SeedlingAnalysisToolCollection<double> Length{
            get
            {
                return _Length;
            }
            set
            {
                _Length = value;
            }
        }

        private SeedlingAnalysisToolCollection<double> _rootLength;
        public SeedlingAnalysisToolCollection<double> rootLength
        {
            get
            {
                return _rootLength;
            }
            set
            {
                _rootLength = value;
            }
        }

        private string _name;
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private SeedlingAnalysisToolCollection<double> _angle;
        public SeedlingAnalysisToolCollection<double> angle
        {
            get
            {
                return _angle;
            }
            set
            {
                _angle = value;
            }
        }

        private int _sampleCount;
        public int samepleCount
        {
            get
            {
                return _sampleCount;
            }
        }

        private bool _include;
        public bool include
        {
            get
            {
                return _include;
            }
            set
            {
                _include = value;
            }
        }
#endregion

#region "Constructor/Destructor"
        public Seedling(string rootName, int count, Point location)
        {
            name = rootName;
            _sampleCount = count;
            _Length = new SeedlingAnalysisToolCollection<double>(count);
            _rootLength = new SeedlingAnalysisToolCollection<double>(count);
            _angle = new SeedlingAnalysisToolCollection<double>(count);
            _timestamp = new SeedlingAnalysisToolCollection<DateTime>(count);
            _location = location;
            _include = false;
        }

        ~Seedling()
        {
            _Length = null;
            _rootLength = null;
            _angle = null;
            _sampleCount = 0;
            name = "";
        }
#endregion

    }
}
