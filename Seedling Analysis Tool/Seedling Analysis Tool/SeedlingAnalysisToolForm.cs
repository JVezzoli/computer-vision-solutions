﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Threading;

namespace Seedling_Analysis_Tool
{
    public partial class SeedlingAnalysisToolForm : Form
    {

#region "Properties"
        private FolderBrowserDialog fbd;
        private string folderName;
        private SaveFileDialog sfd;
        private string outputFilename;
        private FileInfo[] fileList;
        private string[] filenames;
        private TabPage[] tabPages;
        private int currentIndex;
        private Measurement calculations;
        private int startImageIndex;
        private int stopImageIndex;
        private int interval;
        private ProgressBar progBar;
        private int currentPetriDish = -1;
        TraceSwitch ts = new TraceSwitch("Trace Switch", "Main Form");
        private Measurement.RotationOptions _rotate;

#endregion

#region "Initialization/Form Close"
        public SeedlingAnalysisToolForm()
        {
            if (Thread.CurrentThread.Name == null)
                Thread.CurrentThread.Name = "Main";

            InitializeComponent();
            
            //save file dialog setup
            sfd = new SaveFileDialog();
            sfd.DefaultExt = "xlsx";
            sfd.Filter = "Excel Workbook (*.xlsx)|*.xlsx";
            sfd.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            
            //folder browser dialog setup
            fbd = new FolderBrowserDialog();
            fbd.Description = "Select the folder containing the stack of seedling images.";
            fbd.RootFolder = Environment.SpecialFolder.MyComputer; 
            
            //wizard tab setup
            currentIndex = 0;
            tabPages = new TabPage[wizardTabControl.TabCount];
            for(int i=0;i<wizardTabControl.TabCount;i++)
                tabPages[i] = wizardTabControl.TabPages[i];
            for (int i = wizardTabControl.TabCount-1; i > 0; i--)
                wizardTabControl.TabPages.RemoveAt(i);
            
            //combo box setup
            rotaionOptionsComboBox.SelectedIndex = 0;

            //progressbar setup
            progBar = new ProgressBar();
        }

        private void SeedlingAnalysisToolForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            openCVConnection cv = openCVConnection.Instance;
            cv.CleanTempDir();//clean up clean up, everybody everywhere!
        }

#endregion

#region "File System Interaction"

        //Select Folder Browse button clicked
        private void browseButton_Click(object sender, EventArgs e)
        {
            try
            {

                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    folderName = fbd.SelectedPath;
                    
                    //get all files from selected folder then filter out the image files
                    DirectoryInfo dirInfo = new DirectoryInfo(folderName);

                    //any other image types need to go here.  maybe make a command line agrs switch to include additional file types
                    string[] extensionArray = { ".png", ".tif", ".tiff", ".gif", ".jpg", ".jpeg", ".jpe", ".jfif", ".bmp", ".dib" };
                    HashSet<string> allowedExtensions = new HashSet<string>(extensionArray, StringComparer.OrdinalIgnoreCase);
                    fileList = Array.FindAll(dirInfo.GetFiles(), f => allowedExtensions.Contains(f.Extension));
                    
                    if (fileList.Length > 0)
                    {
                        Array.Resize(ref filenames, fileList.Count());
                        for(int i =0;i<fileList.Count();i++)
                        {
                            filenames[i] = fileList[i].FullName;
                        }
                        Array.Sort(filenames);
                        folderLocation.Text = folderName;
                        selectStackNextButton();
                    }
                    else
                    {
                        MessageBox.Show("No files found in selected directory.", "No Images Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Error getting images", MessageBoxButtons.OK, MessageBoxIcon.Error);
                folderLocation.Text = "";
                selectStackNextButton();
            }
        }

        //Save file button clicked
        private void browseBtn2_Click(object sender, EventArgs e)
        {
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                outputFilename = sfd.FileName;
                outputFileTextBox.Text = outputFilename;
            }
            analyzeStackAnalyzeButton();
        }

#endregion

#region "Tab Setup"

        private void setupAssignWindow()
        {
            try
            {
                Image img = Image.FromFile(filenames[0]);
                double rawPicWidth, rawPicHeight;
                double picBoxWidth, picBoxHeight;
                double widthScale, heightScale;

                rawPicHeight = img.Height;
                rawPicWidth = img.Width;
                picBoxHeight = petriDishImgBox.Height;
                picBoxWidth = petriDishImgBox.Width;
                widthScale = picBoxWidth / img.Width;
                heightScale = picBoxHeight / img.Height;

                petriDishImgBox.Image.Dispose();
                petriDishImgBox.Image = img;

                assignPetriDish.SuspendLayout();
                for (int i = 0; i < 12; i++)
                {
                    int x, y;
                    if (i < 4)
                    {
                        y = (int)(1.0 / 4.0 * petriDishImgBox.Height + petriDishImgBox.Location.Y);
                    }
                    else if (i < 8)
                    {
                        y = (int)(2.0 / 4.0 * petriDishImgBox.Height + petriDishImgBox.Location.Y);
                    }
                    else
                    {
                        y = (int)(3.0 / 4.0 * petriDishImgBox.Height + petriDishImgBox.Location.Y);
                    }

                    x = i % 4 * petriDishImgBox.Width / 4 + petriDishImgBox.Location.X;


                    ComboBox cb = new ComboBox();
                    cb.DropDownStyle = ComboBoxStyle.DropDownList;
                    cb.Name = i.ToString();
                    cb.Items.Add("Exclude");
                    for (int j = 0; j < mutantStrands.Value; j++)
                        cb.Items.Add("Mutant " + (j + 1));//j+1 to make it 1 based not 0 based.
                    cb.Items.Add("Wild");
                    cb.SelectedIndex = 0;
                    cb.Location = new Point(x, y);
                    cb.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
                    assignPetriDish.Controls.Add(cb);
                    cb.BringToFront();
                }
            }
            catch (Exception err)
            {
                if (UserConsts.ENABLE_DEBUG)
                {
                    Trace.WriteLine(err.ToString());
                    Trace.WriteLine(err.StackTrace);
                }
            }
            finally
            {
                assignPetriDish.ResumeLayout();
            }

        }

        private void setupImageWindow()
        {
            int numberOfImages = filenames.Count();
            startImageIndex = 0;
            stopImageIndex = numberOfImages - 1;

            startTrackBar.Maximum = numberOfImages - 1;
            stopTrackBar.Maximum = numberOfImages - 1;

            startTrackBar.Value = startImageIndex;
            updateSampleImage(startImageIndex, startPictureBox);
            stopTrackBar.Value = stopImageIndex;
            updateSampleImage(stopImageIndex, stopPictureBox);

            startTimeDiff.Text = "0 Minutes";
            stopTimeDiff.Text = stopImageIndex * imageInterval.Value + " Minutes";
        }

        private void updatePetriDishTypes()
        {
            foreach (Control ctrl in assignPetriDish.Controls)
            {
                if (ctrl is ComboBox)
                {
                    int index;
                    index = (Convert.ToInt32(ctrl.Name.ToString()));
                    calculations.petriDishes[index].type = ctrl.Text;
                }
            }
        }

#endregion

#region "Next/Previous/Analyze Button Click Events"

        private void analyzeButton_Click(object sender, EventArgs e)
        {
            progBar.Location = new Point(this.Size.Width / 2, this.Size.Height / 2);
            progBar.Size = new Size(100, 30);
            progBar.Style = ProgressBarStyle.Blocks;
            progBar.Value = 1;
            this.Controls.Add(progBar);
            progBar.BringToFront();
            wizardTabControl.Enabled = false;
            analysisPreviousButton.Enabled = false;
            analyzeWorker.RunWorkerAsync();
        }

        //Handles clicking of all "Next" buttons
        private void nextButton_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                bool onSeedlingTabs = false;

                //sets up tab for selecting image window, time interval and rotation
                if (btn.Name == "stackNextButton")
                {
                    setupImageWindow();
                }

                //disables the form and begins process for creating measurement object
                if (btn.Name == "selectImageNextButton")
                {
                    interval = (int)imageInterval.Value;
                    progBar.Location = new Point(this.Size.Width / 2, this.Size.Height / 2);
                    progBar.Size = new Size(100, 30);
                    progBar.Style = ProgressBarStyle.Marquee;
                    this.Controls.Add(progBar);
                    progBar.BringToFront();
                    petriDishImgBox.Image.Dispose();
                    petriDishImgBox.Image = Image.FromFile(fileList[0].FullName);
                    wizardTabControl.Enabled = false;
                    _rotate = (Measurement.RotationOptions)rotaionOptionsComboBox.SelectedIndex;
                    detectDishesWorker.RunWorkerAsync();
                }


                //updates petri dishes with selected labels and does initial setup for seedling tab
                if (btn.Name == "dishTypeNextButton")
                {
                    updatePetriDishTypes();
                    currentPetriDish = 0;
                    setupSeedlingTab(currentPetriDish, openCVConnection.colorDishes[currentPetriDish]);
                }

                //Set up next tab to select seedlings in a petri dish image
                //sets image and adds check boxes to correct locations
                if (btn.Name == "assignSeedlingNext")
                {
                    processSeedlingView();
                    currentPetriDish++;
                    if (currentPetriDish < calculations.petriDishes.Count())
                        onSeedlingTabs = setupSeedlingTab(currentPetriDish, openCVConnection.colorDishes[currentPetriDish]);
                    else
                        onSeedlingTabs = false;
                    if (currentPetriDish > calculations.petriDishes.Count() - 1)
                    {
                        onSeedlingTabs = false;
                    }
                }

                //switches to next tab for wizard if not a seedling tab
                if (!onSeedlingTabs && currentIndex + 1 < tabPages.Length)
                {
                    wizardTabControl.SuspendLayout();
                    wizardTabControl.TabPages.RemoveAt(0);
                    currentIndex++;
                    wizardTabControl.TabPages.Add(tabPages[currentIndex]);
                    wizardTabControl.ResumeLayout();
                }

            }
            catch (Exception err)
            {
                if (UserConsts.ENABLE_DEBUG)
                {
                    Trace.WriteLine(err.ToString());
                    Trace.WriteLine(err.StackTrace);
                }
            }
        }
        
        //Handles clicking of all "Previous" buttons
        private void previousButton_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                bool onSeedlingTab = false;

                //Previous button on analyze tab, goes back to seedling selection tabs
                if (btn.Name == "analysisPreviousButton")
                {
                    currentPetriDish = calculations.petriDishes.Count() - 1;
                    setupSeedlingTab(currentPetriDish, openCVConnection.colorDishes[currentPetriDish]);
                }

                //previous buttons on seedling selection tabs
                if (btn.Name == "assignSeedlingsPrevious")
                {
                    processSeedlingView();
                    currentPetriDish--;
                    if (currentPetriDish >= 0)
                        onSeedlingTab = setupSeedlingTab(currentPetriDish, openCVConnection.colorDishes[currentPetriDish]);
                    else
                        onSeedlingTab = false;
                    if (currentPetriDish < 0)
                    {
                        onSeedlingTab = false;
                    }
                }

                //all, will do nothing if we are cycling through seedling selections
                if (!onSeedlingTab && currentIndex - 1 >= 0)
                {
                    wizardTabControl.SuspendLayout();
                    wizardTabControl.TabPages.RemoveAt(0);
                    currentIndex--;
                    wizardTabControl.TabPages.Add(tabPages[currentIndex]);
                    wizardTabControl.ResumeLayout();
                }
            }
            catch (Exception err)
            {
                if (UserConsts.ENABLE_DEBUG)
                {
                    Trace.WriteLine(err.ToString());
                    Trace.WriteLine(err.StackTrace);
                }
            }
        }
#endregion

#region "Next Button Enable/Disable Routines"

        private void analyzeStackAnalyzeButton()
        {
            if (outputFileTextBox.Text.Length > 0)
            {
                analyzeButton.Enabled = true;
            }
            else
            {
                analyzeButton.Enabled = false;
            }
        }

        private void petriDishName_TextChanged_1(object sender, EventArgs e)
        {
            selectStackNextButton();
        }

        private void selectStackNextButton()
        {
            if (petriDishName.Text.Length > 0 && seedlingName.Text.Length > 0 && folderLocation.Text.Length > 0)
            {
                stackNextButton.Enabled = true;
            }
            else
            {
                stackNextButton.Enabled = false;
            }
        }

        private void selectImagesAndIntervalNextButton()
        {
            if (imageInterval.Value > 0 )
            {
                selectImageNextButton.Enabled = true;
            }
            else
            {
                selectImageNextButton.Enabled = false;
            }

        }

#endregion

#region "Background Workers"

        private void analyzeWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bgw = (BackgroundWorker)sender;
            double size = stopImageIndex - startImageIndex + 1;
            for (int i = startImageIndex; i <= stopImageIndex; i++)
            {
                if (UserConsts.ENABLE_DEBUG)
                    Trace.TraceInformation(String.Format("Begin measurement of image {0}", i));
                
                calculations.runMeasurements(i, i);

                if (stopImageIndex != 0)
                {
                    bgw.ReportProgress((int)((double)(i + 1) / size * 100));
                }
            }
            calculations.Print(outputFilename);
        }

        private void analyzeWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progBar.Value = e.ProgressPercentage;
        }

        private void analyzeWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Controls.Remove(progBar);
            wizardTabControl.Enabled = true;
            MessageBox.Show("Analysis complete.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void detectDishesWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                calculations = new Measurement(filenames, petriDishName.Text, seedlingName.Text, interval, _rotate);
            }
            catch (Exception err)
            {
                if (UserConsts.ENABLE_DEBUG)
                {
                    Trace.TraceError(err.ToString());
                    Trace.TraceError(err.StackTrace);
                }
            }
        }
        
        private void detectDishesWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Controls.Remove(progBar);
            setupAssignWindow();
            wizardTabControl.Enabled = true;
        }

#endregion

#region "Select Images and Interval"
        private void imageInterval_ValueChanged(object sender, EventArgs e)
        {
            NumericUpDown intervalSpinner = (NumericUpDown)sender;
            interval = (int)intervalSpinner.Value;
            updateTimeDiffLabel(startTrackBar.Value, startTimeDiff);
            updateTimeDiffLabel(stopTrackBar.Value, stopTimeDiff);
        }

        private void startTrackBar_ValueChanged(object sender, EventArgs e)
        {
            TrackBar tb = (TrackBar)sender;
            updateSampleImage(tb.Value, startPictureBox);
            if (tb.Value > stopTrackBar.Value)
                stopTrackBar.Value = tb.Value;
            updateTimeDiffLabel(tb.Value, startTimeDiff);
        }

        private void stopTrackBar_ValueChanged(object sender, EventArgs e)
        {
            TrackBar tb = (TrackBar)sender;
            updateSampleImage(tb.Value, stopPictureBox);
            if (tb.Value < startTrackBar.Value)
                startTrackBar.Value = tb.Value;
            updateTimeDiffLabel(tb.Value, stopTimeDiff);
        }

        private void updateSampleImage(int index, PictureBox picBox)
        {
            if (picBox.Image != null)
                picBox.Image.Dispose();
            picBox.Image = Image.FromFile(filenames[index]);
        }

        private void updateTimeDiffLabel(int index, Label timeDiff)
        {
            timeDiff.Text = index * imageInterval.Value + " Minutes";
        }
#endregion

#region "Seedling Selection"
        
        private void processSeedlingView()
        {
            try
            {
                foreach (Control ctrl in seedlingTabPage.Controls)
                {
                    if (ctrl is CheckBox)
                    {
                        CheckBox cb = (CheckBox)ctrl;
                        int index = Convert.ToInt32(cb.Name);
                        calculations.petriDishes[currentPetriDish].seedlings[index].include = cb.Checked;
                    }
                }
            }
            catch (Exception err)
            {
                if (UserConsts.ENABLE_DEBUG)
                {
                    Trace.WriteLine(err.ToString());
                    Trace.WriteLine(err.StackTrace);
                }
            }
        }

        private void removeSeedlingViewCheckBoxes()
        {
            for (int i = seedlingTabPage.Controls.Count-1; i >= 0; i--)
            {
                if (seedlingTabPage.Controls[i] is CheckBox)
                {
                    seedlingTabPage.Controls.RemoveAt(i);
                }
            }
        }

        private bool setupSeedlingTab(int petriDishNumber, Image img)
        {
            bool status = true;
            seedlingTabPage.SuspendLayout();
            removeSeedlingViewCheckBoxes();
            if (petriDishNumber >= 0 && petriDishNumber < calculations.petriDishes.Count())
            {
                try
                {
                    double rawPicWidth, rawPicHeight;
                    double picBoxWidth, picBoxHeight;
                    double widthScale, heightScale;

                    rawPicHeight = img.Height;
                    rawPicWidth = img.Width;
                    picBoxHeight = assignSeedlingPictureBox.Height;
                    picBoxWidth = assignSeedlingPictureBox.Width;
                    widthScale = picBoxWidth / img.Width;
                    heightScale = picBoxHeight / img.Height;

                    assignSeedlingsPetriDishNameLabel.Text = calculations.petriDishes[petriDishNumber].name;
                    assignSeedlingPictureBox.Image = img;
                    for (int i = 0; i < calculations.petriDishes[petriDishNumber].seedlingCount; i++)
                    {
                        if (calculations.petriDishes[petriDishNumber] == null)
                            continue;
                        int x, y;
                        x = (int)Math.Round((calculations.petriDishes[petriDishNumber].seedlings[i].location.X * widthScale), 0) + assignSeedlingPictureBox.Location.X;
                        y = (int)Math.Round((calculations.petriDishes[petriDishNumber].seedlings[i].location.Y * heightScale), 0) + assignSeedlingPictureBox.Location.Y;
                        CheckBox cb = new CheckBox();
                        cb.Checked = true;
                        cb.Location = new Point(x, y);
                        cb.BackColor = Color.Transparent;
                        cb.AutoSize = false;
                        cb.Width = 14;
                        cb.Height = 14;
                        cb.Anchor = (AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top);
                        cb.Name = i.ToString();
                        cb.Checked = calculations.petriDishes[currentPetriDish].seedlings[i].include;
                        seedlingTabPage.Controls.Add(cb);
                        cb.BringToFront();
                    }

                }
                catch (Exception e)
                {
                    if (ts.TraceError)
                    {
                        if (UserConsts.ENABLE_DEBUG)
                        {
                            Trace.WriteLine(e.ToString());
                            Trace.WriteLine(e.StackTrace);
                        }
                    }
                    status = false;
                }
                finally
                {
                    seedlingTabPage.ResumeLayout();
                }
            }
            else
            {
                seedlingTabPage.ResumeLayout();
                status = false;
            }
            return status;
        }

#endregion

    }
}


