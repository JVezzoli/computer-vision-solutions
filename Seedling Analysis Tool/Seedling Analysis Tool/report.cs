using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Diagnostics;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
//to get officeOpenXml to work properly go to properties
//and change target framework to .Net framework 4

namespace Seedling_Analysis_Tool
{
    class EPPlus
    {

        public void Excel(string file, SeedlingAnalysisToolCollection<PetriDish>  petridishes)
        {
            
            if (File.Exists(file)) File.Delete(file);
            var excel = new ExcelPackage(new FileInfo(file));
            try
            {
                //totalcount is used for indexing lengths for the standard deviation
                int totalcount = 0;
                int begin = 0;
                int end = 0;
                
                Int32 sampleCount = 0;
                for (int d = 0; d < petridishes.Count(); d++)
                {
                    sampleCount = 0;
                    if (petridishes[d].type == "Exclude")
                        continue;
                    int i = 0;
                    int farCol = 0;//, lastRow = 0;

                    
                    int totalTimestamps = petridishes[d].seedlings[d].timestamp.Count();
                    //adds new worksheet
                    var ws = excel.Workbook.Worksheets.Add(petridishes[d].name + " " + petridishes[d].type);
                    //initializes chart
                    var chart = ws.Drawings.AddChart("LineChart", eChartType.Line);
                    var chart2 = ws.Drawings.AddChart("LineChart2", eChartType.Line);
                    //labels for information about seedling
                    for (int t = 0; t < petridishes[d].seedlingCount; t++)
                    {
                        if (!(petridishes[d].seedlings[t].include))
                            continue;
                        sampleCount++;
                        ws.Cells[totalTimestamps + 8, 1].Value = "Lengths";
                        ws.Cells[totalTimestamps + 8, 2].Value = "Angles";
                        if (t == 0)
                        {
                            ws.Cells[1, 5].Value = petridishes[d].seedlings[t].name;
                            ws.Cells[2, 5].Value = "Time";
                            ws.Cells[2, 6].Value = "Length";
                            ws.Cells[2, 7].Value = "Angle";
                            

                            for (int k = 0; k < petridishes[d].seedlings[t].samepleCount; k++ )
                            {
                                ws.Cells[i + 3, 5].Value = String.Format("{0:H:mm:ss}",petridishes[d].seedlings[sampleCount].timestamp[k]);
                                ws.Cells[i + 3, 6].Value = petridishes[d].seedlings[sampleCount].Length[k];
                                ws.Cells[i + 3, 7].Value = petridishes[d].seedlings[sampleCount].angle[k];

                                //for outputting lengths and angles in a group
                                ws.Cells[totalTimestamps + 8 + sampleCount + totalcount + k, 1].Value = petridishes[d].seedlings[sampleCount].Length[k];
                                ws.Cells[totalTimestamps + 8 + sampleCount + totalcount + k, 2].Value = petridishes[d].seedlings[sampleCount].angle[k];
                                //ws.Cells[totalTimestamps + 7 + totalcount + k, 3].Value = String.Format("{0:H:mm:ss}", petridishes[d].seedlings[sampleCount].timestamp[k]);
                                i++;
                            }
                            ws.Column(1).AutoFit();

                        }
                        else
                        {
                            ws.Cells[1, sampleCount + (3 * sampleCount) + 1].Value = petridishes[d].seedlings[t].name;
                            ws.Cells[2, sampleCount + (3 * sampleCount) + 1].Value = "Time";
                            ws.Cells[2, sampleCount + (3 * sampleCount) + 2].Value = "Length";
                            ws.Cells[2, sampleCount + (3 * sampleCount) + 3].Value = "Angle";
 
                            i = 0;
                            for (int k = 0; k < petridishes[d].seedlings[t].samepleCount; k++)
                            {
                                ws.Cells[i + 3, (4 * sampleCount) + 1].Value = String.Format("{0:H:mm:ss}", petridishes[d].seedlings[t].timestamp[k]);
                                ws.Cells[i + 3, (4 * sampleCount) + 2].Value = petridishes[d].seedlings[t].Length[k];
                                ws.Cells[i + 3, (4 * sampleCount) + 3].Value = petridishes[d].seedlings[t].angle[k];

                                ws.Cells[totalTimestamps + 7 + sampleCount + sampleCount + totalcount + k, 1].Value = petridishes[d].seedlings[t].Length[k];
                                ws.Cells[totalTimestamps + 7 + sampleCount + sampleCount + totalcount + k, 2].Value = petridishes[d].seedlings[t].angle[k];
                                //ws.Cells[totalTimestamps + 7 + sampleCount + sampleCount + totalcount + k, 3].Value = String.Format("{0:H:mm:ss}", petridishes[d].seedlings[sampleCount].timestamp[k]);
                                i++;
                            }
                            ws.Column((sampleCount * 4)).AutoFit();
                            
                        }
                        //for calculating average lengths and angles for each timestamp
                        
                        //lastRow = petridishes[d].seedlings[t].samepleCount;

                        
                    }//end for loop of seedlings
                    farCol = petridishes[d].seedlingCount;
                    /*
                    ws.Cells[2, (3 * sampleCount) + 4].Value = "Average Length";
                    ws.Cells[2, (3 * sampleCount) + 5].Value = "Average Angle";
                    ws.Cells[2, (3 * sampleCount) + 6].Value = "Length Standard Error";
                    ws.Cells[2, (3 * sampleCount) + 7].Value = "Angle Standard Error";
                     * */
                    ws.Cells[2, 1].Value = "Average Length";
                    ws.Cells[2, 2].Value = "Average Angle";
                    ws.Cells[2, 3].Value = "Length Standard Error";
                    ws.Cells[2, 4].Value = "Angle Standard Error";

                    int seedref = 0;
                    while (petridishes[d].seedlings[seedref].timestamp.Count() == 0)
                    {
                        seedref++;
                    }
                    int currentTime = 0;
                    for (int m = 0; m < petridishes[d].seedlings[seedref].timestamp.Count(); m++)
                    {
                        double[] avL = new double[petridishes[d].seedlingCount];
                        double[] avA = new double[petridishes[d].seedlingCount];
                        double[] stdevL = new double[petridishes[d].seedlingCount];
                        double[] stdevA = new double[petridishes[d].seedlingCount];

                        for (int g = 0; g < petridishes[d].seedlingCount; g++)
                        {
                            avL[g] = Convert.ToDouble(ws.Cells[m + 3, (g * 4) + 6].Value);
                            avA[g] = Convert.ToDouble(ws.Cells[m + 3, (g * 4) + 7].Value);
                        }
                        double totalL = 0, totalA = 0, averageL = 0, averageA = 0;
                        //for getting the total by row of length and angle data
                        int avecnt = 0;
                        for (int p = 0; p < avL.Length; p++)
                        {
                            if (avL[p] != 0)
                                avecnt++;
                            totalL += Convert.ToDouble(avL[p]);
                            totalA += Convert.ToDouble(avA[p]);
                            
                        }
                        //totalL = avL.Sum();
                        averageL = totalL / avecnt;
                        averageA = totalA / avecnt;
                        /*
                        //for standard deviation
                        for (int z = 0; z < stdevL.Length; z++)
                        {
                            stdevL[z] = Math.Pow((Convert.ToDouble(avL[z]) - averageL), 2);
                            stdevA[z] = Math.Pow((Convert.ToDouble(avA[z]) - averageA), 2);
                        }
                        //final standard deviations
                        double sdevLen = Math.Sqrt(stdevL.Sum() / averageL);
                        double sdevAng = Math.Sqrt(stdevA.Sum() / averageA);
                        //Final standard error of the mean
                        double sterrLen = sdevLen / Math.Sqrt(stdevL.Length);
                        double sterrAng = sdevAng / Math.Sqrt(stdevA.Length);
                        */
                        //begin indicates where to start at the index for calucating standard deviation
                        //totalcount indicates where to end for standard deviation
                        begin = totalTimestamps + 7;
                        totalcount += avecnt;
                        end = totalTimestamps + 7;
                        string rangeA = "";
                        string rangeB = "";
                        int nums = 0;
                        int cnt = avecnt;
                        int ind = 0;
                        for (int q = 0; q <= (avecnt); q++)
                        {
                            nums = begin + totalTimestamps + (q * totalTimestamps) + (1 * currentTime);

                            if (Convert.ToDouble(ws.Cells[nums, 1].Value) != 0)
                            {
                                if (ind > 0 && rangeA != "" && rangeB != "")
                                {
                                    rangeA += ",";
                                    rangeB += ",";
                                }
                                rangeA += "A" + nums;
                                rangeB += "B" + nums;
                                ind++;
                            }
                        }
                        currentTime++;
                        //write averages and standard error of the mean to cells
                        ws.Cells[m + 3, 1].Value = averageL;
                        ws.Cells[m + 3, 2].Value = averageA;
                        ws.Cells[m + 3, 3].Formula = "=STDEV(" + rangeA + ")/SQRT(" + cnt + ")";  //.Value = sterrLen;   = STDEV(range of values)/SQRT(number)
                        ws.Cells[m + 3, 4].Formula = "=STDEV(" + rangeB + ")/SQRT(" + cnt + ")";  //.Value = sterrAng;
                        
                    }
                    
                    ws.Column(1).AutoFit();
                    ws.Column(2).AutoFit();
                    ws.Column(3).AutoFit();
                    ws.Column(4).AutoFit();
                    //creates chart
                    
                    //chart1
                    chart.SetPosition(15, 0, 1, 0);
                    chart.SetSize(600, 300);
                    chart.Title.Text = Convert.ToString(petridishes[d].name) + " Average Length by Timestamp";
                    chart.XAxis.Title.Text = "Time";
                    chart.YAxis.Title.Text = "Length";
                    //var one = ws.Cells[3, ((3 * farCol) + 4), 4, ((3 * farCol) + 4)];
                    //var two = ws.Cells[3, 1, sampleCount + 4, 1];
                    //var two = ws.Cells[3, ((3 * farCol) + 5), 4, ((3 * farCol) + 5)]; //gets range of angles
                    //var one = ws.Cells[3, 1, sampleCount + 3, 1];
                    //var two = ws.Cells[3, 5, sampleCount + 3, 5];
                    var one = ws.Cells[3, 1, petridishes[d].seedlings[0].timestamp.Count() + 2, 1];
                    var two = ws.Cells[3, 5, petridishes[d].seedlings[0].timestamp.Count() + 2, 5];
                    var serie = (ExcelLineChartSerie)chart.Series.Add(one, two);
                    serie.Header = "Length";

                    //chart2
                    chart2.SetPosition(15, 0, 12, 0);
                    chart2.SetSize(600, 300);
                    chart2.Title.Text = Convert.ToString(petridishes[d].name) + " Average Angle by Timestamp";
                    chart2.XAxis.Title.Text = "Time";
                    chart2.YAxis.Title.Text = "Angle";
                    //var three = ws.Cells[3, ((3 * farCol) + 5), 4, ((3 * farCol) + 5)];
                    //var four = ws.Cells[3, 1, sampleCount + 4, 1];
                    var three = ws.Cells[3, 2, petridishes[d].seedlings[0].timestamp.Count() + 2, 2];
                    //var three = ws.Cells[3, 2, sampleCount + 3, 2];
                    var four = ws.Cells[3, 5, petridishes[d].seedlings[0].timestamp.Count() + 2, 5];
                    //var four = ws.Cells[3, 5, sampleCount + 3, 5];
                    var serie2 = (ExcelLineChartSerie)chart2.Series.Add(three, four);
                    serie2.Header = "Angle";
                    
                }//end for loop of petri dishes
                
            }
            catch (Exception err)
            {
                Trace.TraceError(err.ToString());
                Trace.TraceError(err.StackTrace);
            }
            finally
            {
                excel.Save();
                excel.Dispose();
            }
        }//end excel function
   
    }
}
