﻿namespace Seedling_Analysis_Tool
{
    partial class SeedlingAnalysisToolForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SeedlingAnalysisToolForm));
            this.wizardTabControl = new System.Windows.Forms.TabControl();
            this.selectFolder = new System.Windows.Forms.TabPage();
            this.seedlingName = new System.Windows.Forms.TextBox();
            this.petriDishName = new System.Windows.Forms.TextBox();
            this.seedlingNameLabel = new System.Windows.Forms.Label();
            this.petriDishNameLabel = new System.Windows.Forms.Label();
            this.stackNextButton = new System.Windows.Forms.Button();
            this.browseButton = new System.Windows.Forms.Button();
            this.folderLabel = new System.Windows.Forms.Label();
            this.folderLocation = new System.Windows.Forms.TextBox();
            this.selectNamesAndIntervals = new System.Windows.Forms.TabPage();
            this.label3 = new System.Windows.Forms.Label();
            this.rotaionOptionsComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.mutantStrands = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.stopTimeDiff = new System.Windows.Forms.Label();
            this.startTimeDiff = new System.Windows.Forms.Label();
            this.stopTrackBar = new System.Windows.Forms.TrackBar();
            this.startTrackBar = new System.Windows.Forms.TrackBar();
            this.endLabel = new System.Windows.Forms.Label();
            this.startLabel = new System.Windows.Forms.Label();
            this.imageInterval = new System.Windows.Forms.NumericUpDown();
            this.imageIntervalLabel = new System.Windows.Forms.Label();
            this.previousButton = new System.Windows.Forms.Button();
            this.selectImageNextButton = new System.Windows.Forms.Button();
            this.stopPictureBox = new System.Windows.Forms.PictureBox();
            this.startPictureBox = new System.Windows.Forms.PictureBox();
            this.assignPetriDish = new System.Windows.Forms.TabPage();
            this.previousButton3 = new System.Windows.Forms.Button();
            this.dishTypeNextButton = new System.Windows.Forms.Button();
            this.pedtriInstructionLabel = new System.Windows.Forms.Label();
            this.petriDishImgBox = new System.Windows.Forms.PictureBox();
            this.seedlingTabPage = new System.Windows.Forms.TabPage();
            this.assignSeedlingsPrevious = new System.Windows.Forms.Button();
            this.assignSeedlingNext = new System.Windows.Forms.Button();
            this.assignSeedlingsPetriDishNameLabel = new System.Windows.Forms.Label();
            this.assignSeedlingLabel = new System.Windows.Forms.Label();
            this.assignSeedlingPictureBox = new System.Windows.Forms.PictureBox();
            this.analyzeStack = new System.Windows.Forms.TabPage();
            this.browseBtn2 = new System.Windows.Forms.Button();
            this.outputFileTextBox = new System.Windows.Forms.TextBox();
            this.outputFileLabel = new System.Windows.Forms.Label();
            this.analyzeButton = new System.Windows.Forms.Button();
            this.analysisPreviousButton = new System.Windows.Forms.Button();
            this.analyzeWorker = new System.ComponentModel.BackgroundWorker();
            this.detectDishesWorker = new System.ComponentModel.BackgroundWorker();
            this.wizardTabControl.SuspendLayout();
            this.selectFolder.SuspendLayout();
            this.selectNamesAndIntervals.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mutantStrands)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startPictureBox)).BeginInit();
            this.assignPetriDish.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petriDishImgBox)).BeginInit();
            this.seedlingTabPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.assignSeedlingPictureBox)).BeginInit();
            this.analyzeStack.SuspendLayout();
            this.SuspendLayout();
            // 
            // wizardTabControl
            // 
            this.wizardTabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.wizardTabControl.Controls.Add(this.selectFolder);
            this.wizardTabControl.Controls.Add(this.selectNamesAndIntervals);
            this.wizardTabControl.Controls.Add(this.assignPetriDish);
            this.wizardTabControl.Controls.Add(this.seedlingTabPage);
            this.wizardTabControl.Controls.Add(this.analyzeStack);
            this.wizardTabControl.Location = new System.Drawing.Point(12, 12);
            this.wizardTabControl.Name = "wizardTabControl";
            this.wizardTabControl.SelectedIndex = 0;
            this.wizardTabControl.Size = new System.Drawing.Size(633, 390);
            this.wizardTabControl.TabIndex = 0;
            // 
            // selectFolder
            // 
            this.selectFolder.Controls.Add(this.seedlingName);
            this.selectFolder.Controls.Add(this.petriDishName);
            this.selectFolder.Controls.Add(this.seedlingNameLabel);
            this.selectFolder.Controls.Add(this.petriDishNameLabel);
            this.selectFolder.Controls.Add(this.stackNextButton);
            this.selectFolder.Controls.Add(this.browseButton);
            this.selectFolder.Controls.Add(this.folderLabel);
            this.selectFolder.Controls.Add(this.folderLocation);
            this.selectFolder.Location = new System.Drawing.Point(4, 22);
            this.selectFolder.Name = "selectFolder";
            this.selectFolder.Padding = new System.Windows.Forms.Padding(3);
            this.selectFolder.Size = new System.Drawing.Size(625, 364);
            this.selectFolder.TabIndex = 0;
            this.selectFolder.Text = "Select Stack";
            this.selectFolder.UseVisualStyleBackColor = true;
            // 
            // seedlingName
            // 
            this.seedlingName.Location = new System.Drawing.Point(44, 183);
            this.seedlingName.Name = "seedlingName";
            this.seedlingName.Size = new System.Drawing.Size(221, 20);
            this.seedlingName.TabIndex = 6;
            this.seedlingName.Text = "Seedling";
            this.seedlingName.TextChanged += new System.EventHandler(this.petriDishName_TextChanged_1);
            // 
            // petriDishName
            // 
            this.petriDishName.Location = new System.Drawing.Point(44, 124);
            this.petriDishName.Name = "petriDishName";
            this.petriDishName.Size = new System.Drawing.Size(221, 20);
            this.petriDishName.TabIndex = 4;
            this.petriDishName.Text = "Petri Dish";
            this.petriDishName.TextChanged += new System.EventHandler(this.petriDishName_TextChanged_1);
            // 
            // seedlingNameLabel
            // 
            this.seedlingNameLabel.AutoSize = true;
            this.seedlingNameLabel.Location = new System.Drawing.Point(41, 167);
            this.seedlingNameLabel.Name = "seedlingNameLabel";
            this.seedlingNameLabel.Size = new System.Drawing.Size(80, 13);
            this.seedlingNameLabel.TabIndex = 5;
            this.seedlingNameLabel.Text = "Seedling Prefix:";
            // 
            // petriDishNameLabel
            // 
            this.petriDishNameLabel.AutoSize = true;
            this.petriDishNameLabel.Location = new System.Drawing.Point(41, 107);
            this.petriDishNameLabel.Name = "petriDishNameLabel";
            this.petriDishNameLabel.Size = new System.Drawing.Size(84, 13);
            this.petriDishNameLabel.TabIndex = 3;
            this.petriDishNameLabel.Text = "Petri Dish Prefix:";
            // 
            // stackNextButton
            // 
            this.stackNextButton.Enabled = false;
            this.stackNextButton.Location = new System.Drawing.Point(513, 291);
            this.stackNextButton.Name = "stackNextButton";
            this.stackNextButton.Size = new System.Drawing.Size(75, 23);
            this.stackNextButton.TabIndex = 7;
            this.stackNextButton.Text = "Next";
            this.stackNextButton.UseVisualStyleBackColor = true;
            this.stackNextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(406, 60);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(75, 23);
            this.browseButton.TabIndex = 2;
            this.browseButton.Text = "Browse";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // folderLabel
            // 
            this.folderLabel.AutoSize = true;
            this.folderLabel.Location = new System.Drawing.Point(41, 44);
            this.folderLabel.Name = "folderLabel";
            this.folderLabel.Size = new System.Drawing.Size(39, 13);
            this.folderLabel.TabIndex = 0;
            this.folderLabel.Text = "Folder:";
            // 
            // folderLocation
            // 
            this.folderLocation.Location = new System.Drawing.Point(41, 63);
            this.folderLocation.Name = "folderLocation";
            this.folderLocation.ReadOnly = true;
            this.folderLocation.Size = new System.Drawing.Size(359, 20);
            this.folderLocation.TabIndex = 1;
            // 
            // selectNamesAndIntervals
            // 
            this.selectNamesAndIntervals.Controls.Add(this.label3);
            this.selectNamesAndIntervals.Controls.Add(this.rotaionOptionsComboBox);
            this.selectNamesAndIntervals.Controls.Add(this.label2);
            this.selectNamesAndIntervals.Controls.Add(this.mutantStrands);
            this.selectNamesAndIntervals.Controls.Add(this.label1);
            this.selectNamesAndIntervals.Controls.Add(this.stopTimeDiff);
            this.selectNamesAndIntervals.Controls.Add(this.startTimeDiff);
            this.selectNamesAndIntervals.Controls.Add(this.stopTrackBar);
            this.selectNamesAndIntervals.Controls.Add(this.startTrackBar);
            this.selectNamesAndIntervals.Controls.Add(this.endLabel);
            this.selectNamesAndIntervals.Controls.Add(this.startLabel);
            this.selectNamesAndIntervals.Controls.Add(this.imageInterval);
            this.selectNamesAndIntervals.Controls.Add(this.imageIntervalLabel);
            this.selectNamesAndIntervals.Controls.Add(this.previousButton);
            this.selectNamesAndIntervals.Controls.Add(this.selectImageNextButton);
            this.selectNamesAndIntervals.Controls.Add(this.stopPictureBox);
            this.selectNamesAndIntervals.Controls.Add(this.startPictureBox);
            this.selectNamesAndIntervals.Location = new System.Drawing.Point(4, 22);
            this.selectNamesAndIntervals.Name = "selectNamesAndIntervals";
            this.selectNamesAndIntervals.Padding = new System.Windows.Forms.Padding(3);
            this.selectNamesAndIntervals.Size = new System.Drawing.Size(625, 364);
            this.selectNamesAndIntervals.TabIndex = 1;
            this.selectNamesAndIntervals.Text = "Select Images and Interval";
            this.selectNamesAndIntervals.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 184);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(161, 78);
            this.label3.TabIndex = 23;
            this.label3.Text = "The measurements are done \r\nfrom a lower left to upper right.  \r\nPlease select th" +
    "e rotation option \r\nthat needs applied to the image \r\nso that the correct measur" +
    "ment \r\nis done.";
            // 
            // rotaionOptionsComboBox
            // 
            this.rotaionOptionsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.rotaionOptionsComboBox.FormattingEnabled = true;
            this.rotaionOptionsComboBox.Items.AddRange(new object[] {
            "No Rotation",
            "Rotate About Y-Axis",
            "Rotate About X-Axis",
            "Rotate 180º"});
            this.rotaionOptionsComboBox.Location = new System.Drawing.Point(22, 156);
            this.rotaionOptionsComboBox.Name = "rotaionOptionsComboBox";
            this.rotaionOptionsComboBox.Size = new System.Drawing.Size(121, 21);
            this.rotaionOptionsComboBox.TabIndex = 22;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 139);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 13);
            this.label2.TabIndex = 21;
            this.label2.Text = "Rotation Options";
            // 
            // mutantStrands
            // 
            this.mutantStrands.Location = new System.Drawing.Point(22, 103);
            this.mutantStrands.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.mutantStrands.Name = "mutantStrands";
            this.mutantStrands.Size = new System.Drawing.Size(120, 20);
            this.mutantStrands.TabIndex = 20;
            this.mutantStrands.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 13);
            this.label1.TabIndex = 19;
            this.label1.Text = "Number of Mutant Strands";
            // 
            // stopTimeDiff
            // 
            this.stopTimeDiff.AutoSize = true;
            this.stopTimeDiff.Location = new System.Drawing.Point(484, 226);
            this.stopTimeDiff.Name = "stopTimeDiff";
            this.stopTimeDiff.Size = new System.Drawing.Size(66, 13);
            this.stopTimeDiff.TabIndex = 18;
            this.stopTimeDiff.Text = "stopTimeDiff";
            // 
            // startTimeDiff
            // 
            this.startTimeDiff.AutoSize = true;
            this.startTimeDiff.Location = new System.Drawing.Point(252, 226);
            this.startTimeDiff.Name = "startTimeDiff";
            this.startTimeDiff.Size = new System.Drawing.Size(66, 13);
            this.startTimeDiff.TabIndex = 17;
            this.startTimeDiff.Text = "startTimeDiff";
            // 
            // stopTrackBar
            // 
            this.stopTrackBar.Location = new System.Drawing.Point(429, 178);
            this.stopTrackBar.Name = "stopTrackBar";
            this.stopTrackBar.Size = new System.Drawing.Size(159, 45);
            this.stopTrackBar.TabIndex = 16;
            this.stopTrackBar.ValueChanged += new System.EventHandler(this.stopTrackBar_ValueChanged);
            // 
            // startTrackBar
            // 
            this.startTrackBar.Location = new System.Drawing.Point(206, 178);
            this.startTrackBar.Name = "startTrackBar";
            this.startTrackBar.Size = new System.Drawing.Size(159, 45);
            this.startTrackBar.TabIndex = 15;
            this.startTrackBar.ValueChanged += new System.EventHandler(this.startTrackBar_ValueChanged);
            // 
            // endLabel
            // 
            this.endLabel.AutoSize = true;
            this.endLabel.Location = new System.Drawing.Point(426, 22);
            this.endLabel.Name = "endLabel";
            this.endLabel.Size = new System.Drawing.Size(61, 13);
            this.endLabel.TabIndex = 14;
            this.endLabel.Text = "Final Image";
            // 
            // startLabel
            // 
            this.startLabel.AutoSize = true;
            this.startLabel.Location = new System.Drawing.Point(203, 22);
            this.startLabel.Name = "startLabel";
            this.startLabel.Size = new System.Drawing.Size(61, 13);
            this.startLabel.TabIndex = 13;
            this.startLabel.Text = "Start Image";
            // 
            // imageInterval
            // 
            this.imageInterval.Location = new System.Drawing.Point(22, 38);
            this.imageInterval.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.imageInterval.Name = "imageInterval";
            this.imageInterval.Size = new System.Drawing.Size(120, 20);
            this.imageInterval.TabIndex = 0;
            this.imageInterval.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.imageInterval.ValueChanged += new System.EventHandler(this.imageInterval_ValueChanged);
            // 
            // imageIntervalLabel
            // 
            this.imageIntervalLabel.AutoSize = true;
            this.imageIntervalLabel.Location = new System.Drawing.Point(19, 22);
            this.imageIntervalLabel.Name = "imageIntervalLabel";
            this.imageIntervalLabel.Size = new System.Drawing.Size(169, 13);
            this.imageIntervalLabel.TabIndex = 10;
            this.imageIntervalLabel.Text = "Interval Between Images (minutes)";
            // 
            // previousButton
            // 
            this.previousButton.Location = new System.Drawing.Point(37, 291);
            this.previousButton.Name = "previousButton";
            this.previousButton.Size = new System.Drawing.Size(75, 23);
            this.previousButton.TabIndex = 9;
            this.previousButton.Text = "Previous";
            this.previousButton.UseVisualStyleBackColor = true;
            this.previousButton.Click += new System.EventHandler(this.previousButton_Click);
            // 
            // selectImageNextButton
            // 
            this.selectImageNextButton.Location = new System.Drawing.Point(513, 291);
            this.selectImageNextButton.Name = "selectImageNextButton";
            this.selectImageNextButton.Size = new System.Drawing.Size(75, 23);
            this.selectImageNextButton.TabIndex = 10;
            this.selectImageNextButton.Text = "Next";
            this.selectImageNextButton.UseVisualStyleBackColor = true;
            this.selectImageNextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // stopPictureBox
            // 
            this.stopPictureBox.Location = new System.Drawing.Point(429, 38);
            this.stopPictureBox.Name = "stopPictureBox";
            this.stopPictureBox.Size = new System.Drawing.Size(159, 134);
            this.stopPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.stopPictureBox.TabIndex = 12;
            this.stopPictureBox.TabStop = false;
            // 
            // startPictureBox
            // 
            this.startPictureBox.Location = new System.Drawing.Point(206, 38);
            this.startPictureBox.Name = "startPictureBox";
            this.startPictureBox.Size = new System.Drawing.Size(159, 134);
            this.startPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.startPictureBox.TabIndex = 11;
            this.startPictureBox.TabStop = false;
            // 
            // assignPetriDish
            // 
            this.assignPetriDish.Controls.Add(this.previousButton3);
            this.assignPetriDish.Controls.Add(this.dishTypeNextButton);
            this.assignPetriDish.Controls.Add(this.pedtriInstructionLabel);
            this.assignPetriDish.Controls.Add(this.petriDishImgBox);
            this.assignPetriDish.Location = new System.Drawing.Point(4, 22);
            this.assignPetriDish.Name = "assignPetriDish";
            this.assignPetriDish.Padding = new System.Windows.Forms.Padding(3);
            this.assignPetriDish.Size = new System.Drawing.Size(625, 364);
            this.assignPetriDish.TabIndex = 4;
            this.assignPetriDish.Text = "Assign Petri Dish Type";
            this.assignPetriDish.UseVisualStyleBackColor = true;
            // 
            // previousButton3
            // 
            this.previousButton3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.previousButton3.Location = new System.Drawing.Point(37, 291);
            this.previousButton3.Name = "previousButton3";
            this.previousButton3.Size = new System.Drawing.Size(75, 23);
            this.previousButton3.TabIndex = 11;
            this.previousButton3.Text = "Previous";
            this.previousButton3.UseVisualStyleBackColor = true;
            this.previousButton3.Click += new System.EventHandler(this.previousButton_Click);
            // 
            // dishTypeNextButton
            // 
            this.dishTypeNextButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.dishTypeNextButton.Location = new System.Drawing.Point(513, 291);
            this.dishTypeNextButton.Name = "dishTypeNextButton";
            this.dishTypeNextButton.Size = new System.Drawing.Size(75, 23);
            this.dishTypeNextButton.TabIndex = 12;
            this.dishTypeNextButton.Text = "Next";
            this.dishTypeNextButton.UseVisualStyleBackColor = true;
            this.dishTypeNextButton.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // pedtriInstructionLabel
            // 
            this.pedtriInstructionLabel.AutoSize = true;
            this.pedtriInstructionLabel.Location = new System.Drawing.Point(15, 29);
            this.pedtriInstructionLabel.Name = "pedtriInstructionLabel";
            this.pedtriInstructionLabel.Size = new System.Drawing.Size(119, 39);
            this.pedtriInstructionLabel.TabIndex = 1;
            this.pedtriInstructionLabel.Text = "Select which petri dish\r\nis wild, mutant or should\r\nbe excluded.";
            // 
            // petriDishImgBox
            // 
            this.petriDishImgBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.petriDishImgBox.Image = global::Seedling_Analysis_Tool.Properties.Resources._2000_01_01_001;
            this.petriDishImgBox.Location = new System.Drawing.Point(140, 6);
            this.petriDishImgBox.Name = "petriDishImgBox";
            this.petriDishImgBox.Size = new System.Drawing.Size(367, 352);
            this.petriDishImgBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.petriDishImgBox.TabIndex = 0;
            this.petriDishImgBox.TabStop = false;
            // 
            // seedlingTabPage
            // 
            this.seedlingTabPage.Controls.Add(this.assignSeedlingsPrevious);
            this.seedlingTabPage.Controls.Add(this.assignSeedlingNext);
            this.seedlingTabPage.Controls.Add(this.assignSeedlingsPetriDishNameLabel);
            this.seedlingTabPage.Controls.Add(this.assignSeedlingLabel);
            this.seedlingTabPage.Controls.Add(this.assignSeedlingPictureBox);
            this.seedlingTabPage.Location = new System.Drawing.Point(4, 22);
            this.seedlingTabPage.Name = "seedlingTabPage";
            this.seedlingTabPage.Padding = new System.Windows.Forms.Padding(3);
            this.seedlingTabPage.Size = new System.Drawing.Size(625, 364);
            this.seedlingTabPage.TabIndex = 5;
            this.seedlingTabPage.Text = "Assign Seedlings";
            this.seedlingTabPage.UseVisualStyleBackColor = true;
            // 
            // assignSeedlingsPrevious
            // 
            this.assignSeedlingsPrevious.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.assignSeedlingsPrevious.Location = new System.Drawing.Point(37, 291);
            this.assignSeedlingsPrevious.Name = "assignSeedlingsPrevious";
            this.assignSeedlingsPrevious.Size = new System.Drawing.Size(75, 23);
            this.assignSeedlingsPrevious.TabIndex = 14;
            this.assignSeedlingsPrevious.Text = "Previous";
            this.assignSeedlingsPrevious.UseVisualStyleBackColor = true;
            this.assignSeedlingsPrevious.Click += new System.EventHandler(this.previousButton_Click);
            // 
            // assignSeedlingNext
            // 
            this.assignSeedlingNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.assignSeedlingNext.Location = new System.Drawing.Point(513, 291);
            this.assignSeedlingNext.Name = "assignSeedlingNext";
            this.assignSeedlingNext.Size = new System.Drawing.Size(75, 23);
            this.assignSeedlingNext.TabIndex = 15;
            this.assignSeedlingNext.Text = "Next";
            this.assignSeedlingNext.UseVisualStyleBackColor = true;
            this.assignSeedlingNext.Click += new System.EventHandler(this.nextButton_Click);
            // 
            // assignSeedlingsPetriDishNameLabel
            // 
            this.assignSeedlingsPetriDishNameLabel.AutoSize = true;
            this.assignSeedlingsPetriDishNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assignSeedlingsPetriDishNameLabel.Location = new System.Drawing.Point(9, 21);
            this.assignSeedlingsPetriDishNameLabel.Name = "assignSeedlingsPetriDishNameLabel";
            this.assignSeedlingsPetriDishNameLabel.Size = new System.Drawing.Size(100, 20);
            this.assignSeedlingsPetriDishNameLabel.TabIndex = 2;
            this.assignSeedlingsPetriDishNameLabel.Text = "PetriDish###";
            // 
            // assignSeedlingLabel
            // 
            this.assignSeedlingLabel.Location = new System.Drawing.Point(10, 52);
            this.assignSeedlingLabel.Name = "assignSeedlingLabel";
            this.assignSeedlingLabel.Size = new System.Drawing.Size(124, 47);
            this.assignSeedlingLabel.TabIndex = 1;
            this.assignSeedlingLabel.Text = "Check any seedlings that were detected.";
            // 
            // assignSeedlingPictureBox
            // 
            this.assignSeedlingPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.assignSeedlingPictureBox.Image = global::Seedling_Analysis_Tool.Properties.Resources.petri_dish;
            this.assignSeedlingPictureBox.Location = new System.Drawing.Point(140, 6);
            this.assignSeedlingPictureBox.Name = "assignSeedlingPictureBox";
            this.assignSeedlingPictureBox.Size = new System.Drawing.Size(367, 352);
            this.assignSeedlingPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.assignSeedlingPictureBox.TabIndex = 13;
            this.assignSeedlingPictureBox.TabStop = false;
            // 
            // analyzeStack
            // 
            this.analyzeStack.Controls.Add(this.browseBtn2);
            this.analyzeStack.Controls.Add(this.outputFileTextBox);
            this.analyzeStack.Controls.Add(this.outputFileLabel);
            this.analyzeStack.Controls.Add(this.analyzeButton);
            this.analyzeStack.Controls.Add(this.analysisPreviousButton);
            this.analyzeStack.Location = new System.Drawing.Point(4, 22);
            this.analyzeStack.Name = "analyzeStack";
            this.analyzeStack.Padding = new System.Windows.Forms.Padding(3);
            this.analyzeStack.Size = new System.Drawing.Size(625, 364);
            this.analyzeStack.TabIndex = 3;
            this.analyzeStack.Text = "Analyze Stack";
            this.analyzeStack.UseVisualStyleBackColor = true;
            // 
            // browseBtn2
            // 
            this.browseBtn2.Location = new System.Drawing.Point(402, 37);
            this.browseBtn2.Name = "browseBtn2";
            this.browseBtn2.Size = new System.Drawing.Size(75, 23);
            this.browseBtn2.TabIndex = 10;
            this.browseBtn2.Text = "Browse";
            this.browseBtn2.UseVisualStyleBackColor = true;
            this.browseBtn2.Click += new System.EventHandler(this.browseBtn2_Click);
            // 
            // outputFileTextBox
            // 
            this.outputFileTextBox.Location = new System.Drawing.Point(37, 39);
            this.outputFileTextBox.Name = "outputFileTextBox";
            this.outputFileTextBox.ReadOnly = true;
            this.outputFileTextBox.Size = new System.Drawing.Size(359, 20);
            this.outputFileTextBox.TabIndex = 9;
            // 
            // outputFileLabel
            // 
            this.outputFileLabel.AutoSize = true;
            this.outputFileLabel.Location = new System.Drawing.Point(34, 23);
            this.outputFileLabel.Name = "outputFileLabel";
            this.outputFileLabel.Size = new System.Drawing.Size(61, 13);
            this.outputFileLabel.TabIndex = 8;
            this.outputFileLabel.Text = "Output File:";
            // 
            // analyzeButton
            // 
            this.analyzeButton.Enabled = false;
            this.analyzeButton.Location = new System.Drawing.Point(513, 291);
            this.analyzeButton.Name = "analyzeButton";
            this.analyzeButton.Size = new System.Drawing.Size(75, 23);
            this.analyzeButton.TabIndex = 7;
            this.analyzeButton.Text = "Analyze";
            this.analyzeButton.UseVisualStyleBackColor = true;
            this.analyzeButton.Click += new System.EventHandler(this.analyzeButton_Click);
            // 
            // analysisPreviousButton
            // 
            this.analysisPreviousButton.Location = new System.Drawing.Point(37, 291);
            this.analysisPreviousButton.Name = "analysisPreviousButton";
            this.analysisPreviousButton.Size = new System.Drawing.Size(75, 23);
            this.analysisPreviousButton.TabIndex = 6;
            this.analysisPreviousButton.Text = "Previous";
            this.analysisPreviousButton.UseVisualStyleBackColor = true;
            this.analysisPreviousButton.Click += new System.EventHandler(this.previousButton_Click);
            // 
            // analyzeWorker
            // 
            this.analyzeWorker.WorkerReportsProgress = true;
            this.analyzeWorker.WorkerSupportsCancellation = true;
            this.analyzeWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.analyzeWorker_DoWork);
            this.analyzeWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.analyzeWorker_ProgressChanged);
            this.analyzeWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.analyzeWorker_RunWorkerCompleted);
            // 
            // detectDishesWorker
            // 
            this.detectDishesWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.detectDishesWorker_DoWork);
            this.detectDishesWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.detectDishesWorker_RunWorkerCompleted);
            // 
            // SeedlingAnalysisToolForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 414);
            this.Controls.Add(this.wizardTabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SeedlingAnalysisToolForm";
            this.Text = "Seedling Analysis Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SeedlingAnalysisToolForm_FormClosing);
            this.wizardTabControl.ResumeLayout(false);
            this.selectFolder.ResumeLayout(false);
            this.selectFolder.PerformLayout();
            this.selectNamesAndIntervals.ResumeLayout(false);
            this.selectNamesAndIntervals.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mutantStrands)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stopPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startPictureBox)).EndInit();
            this.assignPetriDish.ResumeLayout(false);
            this.assignPetriDish.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.petriDishImgBox)).EndInit();
            this.seedlingTabPage.ResumeLayout(false);
            this.seedlingTabPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.assignSeedlingPictureBox)).EndInit();
            this.analyzeStack.ResumeLayout(false);
            this.analyzeStack.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl wizardTabControl;
        private System.Windows.Forms.TabPage selectFolder;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.Label folderLabel;
        private System.Windows.Forms.TextBox folderLocation;
        private System.Windows.Forms.TabPage selectNamesAndIntervals;
        private System.Windows.Forms.TabPage analyzeStack;
        private System.Windows.Forms.Button stackNextButton;
        private System.Windows.Forms.Button selectImageNextButton;
        private System.Windows.Forms.Button previousButton;
        private System.Windows.Forms.Button analyzeButton;
        private System.Windows.Forms.Button analysisPreviousButton;
        private System.Windows.Forms.NumericUpDown imageInterval;
        private System.Windows.Forms.Label imageIntervalLabel;
        private System.Windows.Forms.TextBox seedlingName;
        private System.Windows.Forms.TextBox petriDishName;
        private System.Windows.Forms.Label seedlingNameLabel;
        private System.Windows.Forms.Label petriDishNameLabel;
        private System.Windows.Forms.TabPage assignPetriDish;
        private System.Windows.Forms.Button previousButton3;
        private System.Windows.Forms.Button dishTypeNextButton;
        private System.Windows.Forms.Label pedtriInstructionLabel;
        private System.Windows.Forms.PictureBox petriDishImgBox;
        private System.Windows.Forms.Button browseBtn2;
        private System.Windows.Forms.TextBox outputFileTextBox;
        private System.Windows.Forms.Label outputFileLabel;
        private System.ComponentModel.BackgroundWorker analyzeWorker;
        private System.Windows.Forms.Label endLabel;
        private System.Windows.Forms.Label startLabel;
        private System.Windows.Forms.PictureBox stopPictureBox;
        private System.Windows.Forms.PictureBox startPictureBox;
        private System.Windows.Forms.TrackBar stopTrackBar;
        private System.Windows.Forms.TrackBar startTrackBar;
        private System.Windows.Forms.Label stopTimeDiff;
        private System.Windows.Forms.Label startTimeDiff;
        private System.Windows.Forms.NumericUpDown mutantStrands;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker detectDishesWorker;
        private System.Windows.Forms.TabPage seedlingTabPage;
        private System.Windows.Forms.Button assignSeedlingsPrevious;
        private System.Windows.Forms.Button assignSeedlingNext;
        private System.Windows.Forms.PictureBox assignSeedlingPictureBox;
        private System.Windows.Forms.Label assignSeedlingsPetriDishNameLabel;
        private System.Windows.Forms.Label assignSeedlingLabel;
        private System.Windows.Forms.ComboBox rotaionOptionsComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        
    }
}

