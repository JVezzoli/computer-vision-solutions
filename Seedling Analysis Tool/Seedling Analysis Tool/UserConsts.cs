﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace Seedling_Analysis_Tool
{
    static class UserConsts
    {
#region "User Settable Constants"
        public static int MIN_SEEDLING_LEGTH_X;//default to 50
        public static int MAX_SEEDLING_LENGTH_X;//default to 200
        public static int MAX_SEEDLING_LENGTH_Y;//80
        public static int MAX_GAP_SIZE;//default to 5
        public static int BLACK_RED_VALUE;//default to 20
        public static int BLACK_GREEN_VALUE;//default to 20
        public static int BLACK_BLUE_VALUE;//default to 20
        public static int NEW_START_LOCATION_DISTANCE;//default to 30
        public static int RESIZE_IMAGE_WIDTH;//default to 1330
        public static int RESIZE_IMAGE_HEIGHT;//default to 1000
        public static int MIN_DISTANCE_BETWEEN_SEEDLINGS;//default to 30
        public static double RADIANS;//default to 57.2957795
        public static double PETRI_DISH_OUTER_DIAMETER;//default to 93
        public static int SEEDLING_TRACKING_MAX_SEARCH_DISTANCE;//defualt to 15
        public static double  TRACKING_PERCENT_MIN;//default to 50%
        public static bool FORCE_INCREASING_LENGTH;//default to true
        public static bool FORCE_INCREASING_ANGLE;//default to true
        public static bool ENABLE_DEBUG;//default to false
#endregion

        public static void Init()
        {
            if (!File.Exists("UserConsts.txt"))
            {
                File.WriteAllText("UserConsts.txt", Properties.Resources.UserConsts);
            }
            SetConsts();
        }
        private static void SetConsts()
        {
            string line;
            string[] vals;
            System.IO.StreamReader file = new System.IO.StreamReader("UserConsts.txt");
            while ((line = file.ReadLine()) != null)
            {
                vals = line.Split(';');
                if (vals[0].Length > 0)
                {
                    vals = vals[0].Split('=');
                    SetValue(vals[0].Trim(), vals[1].Trim());
                }
            }
            file.Close();
        }
        private static void SetValue(string name, string val)
        {
            switch (name)
            {
                case "MIN_SEEDLING_LEGTH_X":
                    MIN_SEEDLING_LEGTH_X = Convert.ToInt32(val);
                    break;
                case "MAX_SEEDLING_LENGTH_X":
                    MAX_SEEDLING_LENGTH_X = Convert.ToInt32(val);
                    break;
                case "MAX_SEEDLING_LENGTH_Y":
                    MAX_SEEDLING_LENGTH_Y = Convert.ToInt32(val);
                    break;
                case "MAX_GAP_SIZE":
                    MAX_GAP_SIZE = Convert.ToInt32(val);
                    break;
                case "BLACK_RED_VALUE":
                    BLACK_RED_VALUE = Convert.ToInt32(val);
                    break;
                case "BLACK_GREEN_VALUE":
                    BLACK_GREEN_VALUE = Convert.ToInt32(val);
                    break;
                case "BLACK_BLUE_VALUE":
                    BLACK_BLUE_VALUE = Convert.ToInt32(val);
                    break;
                case "NEW_START_LOCATION_DISTANCE":
                    NEW_START_LOCATION_DISTANCE = Convert.ToInt32(val);
                    break;
                case "RESIZE_IMAGE_WIDTH":
                    RESIZE_IMAGE_WIDTH = Convert.ToInt32(val);
                    break;
                case "RESIZE_IMAGE_HEIGHT":
                    RESIZE_IMAGE_HEIGHT = Convert.ToInt32(val);
                    break;
                case "MIN_DISTANCE_BETWEEN_SEEDLINGS":
                    MIN_DISTANCE_BETWEEN_SEEDLINGS = Convert.ToInt32(val);
                    break;
                case "RADIANS":
                    RADIANS = Convert.ToDouble(val);
                    break;
                case "PETRI_DISH_OUTER_DIAMETER":
                    PETRI_DISH_OUTER_DIAMETER = Convert.ToDouble(val);
                    break;
                case "SEEDLING_TRACKING_MAX_SEARCH_DISTANCE":
                    SEEDLING_TRACKING_MAX_SEARCH_DISTANCE = Convert.ToInt32(val);
                    break;
                case "TRACKING_PERCENT_MIN":
                    TRACKING_PERCENT_MIN = Convert.ToDouble(val);
                    break;
                case "FORCE_INCREASING_LENGTH":
                    if (Convert.ToInt32(val) == 0)
                        FORCE_INCREASING_LENGTH = false;
                    else
                        FORCE_INCREASING_LENGTH = true;
                    break;
                case "FORCE_INCREASING_ANGLE":
                    if (Convert.ToInt32(val) == 0)
                        FORCE_INCREASING_ANGLE = false;
                    else
                        FORCE_INCREASING_ANGLE = true;
                    break;
                case "ENABLE_DEBUG":
                    if (Convert.ToInt32(val) == 0)
                        ENABLE_DEBUG = false;
                    else
                        ENABLE_DEBUG = true;
                    break;
                default:
                    if (UserConsts.ENABLE_DEBUG)
                        Trace.TraceWarning("{0} is not a valid user constant", name);
                    break;
            }

        }
    }
}
