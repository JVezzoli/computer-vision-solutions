﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;


namespace Seedling_Analysis_Tool
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            try
            {
                UserConsts.Init();
                if (UserConsts.ENABLE_DEBUG)
                    Trace.AutoFlush = true;
                Application.Run(new SeedlingAnalysisToolForm());
            }
            catch (Exception err)
            {
                if (UserConsts.ENABLE_DEBUG)
                {
                    Trace.TraceError(err.ToString());
                    Trace.TraceError(err.StackTrace);
                }
            }
        }
    }
}
