﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Seedling_Analysis_Tool
{
    class PetriDish
    {

#region "Properties"
        private double _baseImageHeight;
        public double baseImageHeight
        {
            get
            {
                return _baseImageHeight;
            }
            set
            {
                _baseImageHeight = value;
            }
        }

        private string _type;
        public string type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

	    private string _name;
        public string name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        private double _mmPerPixel;
        public double mmPerPixel
        {
            get
            {
                return _mmPerPixel;
            }
        }

        private SeedlingAnalysisToolCollection<Seedling> _seedlings;
        public SeedlingAnalysisToolCollection<Seedling> seedlings
        {
            get
            {
                return _seedlings;
            }
            set
            {
                _seedlings = value;
            }
        }

        private int _seedlingCount;
        public int seedlingCount
        {
            get
            {
                return _seedlingCount;
            }
        }
#endregion

#region "Constructor/Destructor"
        public PetriDish(string petriDishRootName, double lengthScalar, int seedlingCount, string seedlingRootName, int sampleCount, Point[] seedlingLocations, double height)
        {
            _baseImageHeight = height;
            name = petriDishRootName;
            _mmPerPixel = lengthScalar;
            _seedlingCount = seedlingCount;
            _seedlings = new SeedlingAnalysisToolCollection<Seedling>(seedlingCount);
            string nameSuffix;
            if (_mmPerPixel > 1000)
                type = "Exclude";
            for(int i = 0;i<seedlingCount;i++)
            {
                nameSuffix = i.ToString();
                nameSuffix = nameSuffix.PadLeft(3, '0');
                _seedlings[i] = new Seedling(seedlingRootName+nameSuffix, sampleCount,seedlingLocations[i]);
            }
        }

        ~PetriDish()
        {
            for (int i = seedlings.Count() - 1; i >= 0; i--)
            {
                _seedlings = null;
            }
        }
#endregion

    }
}
